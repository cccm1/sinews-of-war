﻿
ethos_bellicose = {
	type = ethos
	character_modifier = {
		monthly_martial_lifestyle_xp_gain_mult = 0.1
		monthly_intrigue_lifestyle_xp_gain_mult = 0.1	
	
		men_at_arms_recruitment_cost = -0.1
		men_at_arms_maintenance = -0.1
		
		ai_war_chance = 0.5
		ai_war_cooldown = -0.25
		
		ai_boldness = low_positive_ai_value
	}

	culture_modifier = {
		mercenary_count_mult = 0.5
	}
	parameters = {		
		martial_education_better_outcomes = yes
		intrigue_education_better_outcomes = yes
		rowdy_trait_more_common = yes
		
		unlock_warlike_court = yes
		unlock_intrigue_court = yes
	}

	ai_will_do = {
		value = 1
	}
}

ethos_stoic = {
	type = ethos
	character_modifier = {	
		monthly_martial_lifestyle_xp_gain_mult = 0.1
		monthly_diplomacy_lifestyle_xp_gain_mult = 0.1
		
		stress_gain_mult = -0.15
		hostile_county_attrition = -0.2
		negate_health_penalty_add = 0.25
	}
	parameters = {
		martial_education_better_outcomes = yes
		diplomacy_education_better_outcomes = yes
		
		rowdy_trait_more_common = yes
		charming_trait_more_common = yes

		unlock_warlike_court = yes
		unlock_diplomatic_court = yes		
	}
	province_modifier = {
		hostile_raid_time = 0.25
	}

	ai_will_do = {
		value = 1
	}
}

ethos_bureaucratic = {
	type = ethos
	character_modifier = {
		monthly_martial_lifestyle_xp_gain_mult = 0.1
		monthly_stewardship_lifestyle_xp_gain_mult = 0.1
		
		cultural_head_fascination_mult = 0.1
		
		ai_energy = low_positive_ai_value
	}
	county_modifier = {
		development_growth_factor = 0.1
	}
	parameters = {
		martial_education_better_outcomes = yes
		stewardship_education_better_outcomes = yes
		
		bossy_trait_more_common = yes
		rowdy_trait_less_common = yes
		curious_trait_less_common = yes
		
		unlock_warlike_court = yes
		unlock_administrative_court = yes
	}

	ai_will_do = {
		value = 1
	}
}

ethos_spiritual = {
	type = ethos
	character_modifier = {
		monthly_learning_lifestyle_xp_gain_mult = 0.1	
		monthly_stewardship_lifestyle_xp_gain_mult = 0.1	
	
		
		monthly_county_control_change_factor = 0.15
		monthly_piety_gain_mult = 0.15
		faith_creation_piety_cost_mult = -0.2
		
		ai_zeal = low_positive_ai_value
	}
	parameters = {
		learning_education_better_outcomes = yes
		stewardship_education_better_outcomes = yes
		
		pensive_trait_more_common = yes
		rowdy_trait_less_common = yes
		charming_trait_less_common = yes

		unlock_scholarly_court = yes
		unlock_administrative_court = yes
		
		demd_meta_vigor_mult_500 = yes
	}
	ai_will_do = {
		value = 1
	}
}

ethos_courtly = {
	type = ethos
	desc = ethos_courtly_desc_nhc # To circumvent a hash collision with tenet_ritual_cannibalism_desc
	character_modifier = {
		monthly_learning_lifestyle_xp_gain_mult = 0.1
		monthly_diplomacy_lifestyle_xp_gain_mult = 0.1
		
		monthly_prestige_gain_mult = 0.15
		monthly_court_grandeur_change_mult = 0.15	
		title_creation_cost_mult = -0.15
		
		ai_amenity_target_baseline = 0.2
		ai_amenity_spending = 0.25		
	}
	parameters = {
		learning_education_better_outcomes = yes
		diplomacy_education_better_outcomes = yes

		curious_trait_more_common = yes
		bossy_trait_less_common = yes
		rowdy_trait_less_common = yes
		
		unlock_scholarly_court = yes
		unlock_diplomatic_court = yes		
	}

	ai_will_do = {
		value = 1
		if = {
			limit = {
				scope:character = {
					NAND = {
						highest_held_title_tier >= tier_kingdom
						OR = {
							has_government = feudal_government
							has_government = clan_government
						}
					}
				}
			}
			multiply = 0
		}
	}
}

ethos_egalitarian = {
	type = ethos
	#culture_modifier = {
	#	cultural_acceptance_gain_mult = 0.35
	#}
	character_modifier = {
		monthly_diplomacy_lifestyle_xp_gain_mult = 0.1	
		monthly_intrigue_lifestyle_xp_gain_mult = 0.1
		
		owned_personal_scheme_success_chance_add = 10
		enemy_hostile_scheme_success_chance_add = 10
					
		opinion_of_different_culture = 5
		opinion_of_different_faith = 5
	}
	parameters = {
		diplomacy_education_better_outcomes = yes
		intrigue_education_better_outcomes = yes
		
		charming_trait_more_common = yes
		bossy_trait_less_common = yes
		pensive_trait_less_common = yes
		
		unlock_diplomatic_court	= yes	
		unlock_intrigue_court = yes
		
		demd_meta_vigor_multneg_500 = yes
	}

	ai_will_do = {
		value = 1
	}
}

ethos_communal = {
	type = ethos	
	character_modifier = {	
		monthly_stewardship_lifestyle_xp_gain_mult = 0.1	
		monthly_intrigue_lifestyle_xp_gain_mult = 0.1	
	
		close_relative_opinion = 5
		same_culture_mercenary_hire_cost_mult = -0.15

		opinion_of_different_culture = -5	
	}
	parameters = {
		stewardship_education_better_outcomes = yes	
		intrigue_education_better_outcomes = yes

		rowdy_trait_more_common = yes
		bossy_trait_more_common = yes

		unlock_administrative_court = yes
		unlock_intrigue_court = yes
		
		demd_meta_vigor_mult_500 = yes
	}
	province_modifier = {
		build_gold_cost = -0.1
		build_speed = -0.1
	}

	ai_will_do = {
		value = 1
	}
}
