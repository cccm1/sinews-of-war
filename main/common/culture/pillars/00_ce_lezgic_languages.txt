﻿
language_lezgic = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_lezgic
		}
	}
	parameters = {
		language_group_karto_zan = yes
		language_family_kartvelian = yes
		language_union_caucasus = yes
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_lezgic }
			multiply = same_language_choice_factor
		}
		else_if = {
			limit = { has_cultural_parameter = language_group_lezgic }
			multiply = same_language_group_choice_factor
		}
		else_if = {
			limit = { has_cultural_parameter = language_family_east_caucasian }
			multiply = same_language_family_choice_factor
		}
		else_if = {
			limit = { has_cultural_parameter = language_union_caucasus }
			multiply = same_language_union_choice_factor
		}
	}
	
	color = { 0.65 0.92 0.55 }
}

language_udi = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_udi
		}
	}
	parameters = {
		language_group_lezgic = yes
		language_family_east_caucasian = yes
		language_union_caucasus = yes
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_udi }
			multiply = same_language_choice_factor
		}
		else_if = {
			limit = { has_cultural_parameter = language_group_lezgic }
			multiply = same_language_group_choice_factor
		}
		else_if = {
			limit = { has_cultural_parameter = language_family_east_caucasian }
			multiply = same_language_family_choice_factor
		}
		else_if = {
			limit = { has_cultural_parameter = language_union_caucasus }
			multiply = same_language_union_choice_factor
		}
	}
	
	color = { 0.65 0.94 0.55 }
}

language_avaric = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_avaric
		}
	}
	parameters = {
		language_group_avar_andic = yes
		language_family_east_caucasian = yes 
		language_union_caucasus = yes
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_avaric }
			multiply = same_language_choice_factor
		}
		else_if = {
			limit = { has_cultural_parameter = language_group_avar_andic }
			multiply = same_language_group_choice_factor
		}
		else_if = {
			limit = { has_cultural_parameter = language_family_east_caucasian }
			multiply = same_language_family_choice_factor
		}
		else_if = {
			limit = { has_cultural_parameter = language_union_caucasus }
			multiply = same_language_union_choice_factor
		}
	}
	
	color = { 0.65 0.96 0.55 }
}

