﻿language_haratine = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_haratine
		}
	}
	parameters = {
		language_group_semitic = yes
		language_family_afro_asiatic = yes 		
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_haratine }
			multiply = same_language_choice_factor
		}
		else_if = {
			limit = { has_cultural_parameter = language_group_semitic }
			multiply = same_language_group_choice_factor
		}
		else_if = {
			limit = { has_cultural_parameter = language_family_afro_asiatic }
			multiply = same_language_family_choice_factor
		}
		if = {
			limit = {
				scope:character = {
					religion = religion:islam_religion
				}
			}
			multiply = same_language_union_choice_factor
		}
	}
	
	color = { 0.63 0.26 0.01 }
}

language_afariqa = {
	type = language
	is_shown = {
		language_is_shown_trigger = {
			LANGUAGE = language_afariqa
		}
	}
	parameters = {
		language_group_romance = yes
		language_family_indo_european = yes 		
	}
	ai_will_do = {
		value = 10
		if = {
			limit = { has_cultural_pillar = language_afariqa }
			multiply = same_language_choice_factor
		}
		else_if = {
			limit = { has_cultural_parameter = language_group_romance }
			multiply = same_language_group_choice_factor
		}
		else_if = {
			limit = { has_cultural_parameter = language_family_indo_european }
			multiply = same_language_family_choice_factor
		}
	}
	
	color = { 3.63 0.26 3.01 }
}