package main

import (
	"fmt"
	"os"
	"path/filepath"
	"strconv"
	"time"
)

func main() {
	fmt.Println()
	fmt.Println("#################################################")
	fmt.Println("CCU Patcher")
	fmt.Println("By Vertimnus")
	currentTime := time.Now()
	fmt.Println("Compiled " + currentTime.Month().String() + " " + strconv.Itoa(currentTime.Day()) + ", " + strconv.Itoa(currentTime.Year()))
	fmt.Println("#################################################")
	fmt.Println()
	time.Sleep(100)

	path, _ := os.Getwd()
	modDirectory := filepath.Join(filepath.Dir(path),"ek2")
	sourceDirectory := "C:\\Program Files (x86)\\Steam\\steamapps\\workshop\\content\\1158310\\2887120253"

	writeTenetScriptedEffect(modDirectory, "demd_meta_tenets.txt")
	writeTraditionScriptedEffect(modDirectory, "demd_meta_traditions.txt")
	writePillarScriptedEffect(modDirectory, "demd_meta_pillars.txt")

	writeCCUFiles(modDirectory)
	onActionPrinter(modDirectory)
	titleAssignment(modDirectory, sourceDirectory)
	bakEliminator(modDirectory)

	provinceFunction(modDirectory, sourceDirectory, "demd_sea_province_setup.txt")
}

func writeHeader(outFile *os.File) {
	_, _ = outFile.WriteString("\ufeff")
	_, _ = outFile.WriteString("#############################################\n")
	_, _ = outFile.WriteString("# EK2 Unbound Patcher\n")
	_, _ = outFile.WriteString("# by Vertimnus\n")
	_, _ = outFile.WriteString("# This file was compiled by a machine.\n")
	_, _ = outFile.WriteString("# It should never be manually edited.\n")
	_, _ = outFile.WriteString("#############################################\n\n")
}

func writeLocHeader(outFile *os.File) {
	_, _ = outFile.WriteString("\ufeff")
	_, _ = outFile.WriteString("l_english:\n\n")
	_, _ = outFile.WriteString("#############################################\n")
	_, _ = outFile.WriteString("# EK2 Unbound Patcher\n")
	_, _ = outFile.WriteString("# by Vertimnus\n")
	_, _ = outFile.WriteString("# This file was compiled by a machine.\n")
	_, _ = outFile.WriteString("# It should never be manually edited.\n")
	_, _ = outFile.WriteString("#############################################\n\n")
}