package main

import (
	"bufio"
	"fmt"
	"image/png"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

func provinceFunction(modDir string, sourceDir, outName string) {
	mapDataFolder := filepath.Join(sourceDir, "map_data")
	outDir := filepath.Join(modDir, "common", "scripted_effects")
	fmt.Println("processing definition.csv")
	provinces, color2provID := getProvinces(mapDataFolder)
	fmt.Println("processing provinces.png")
	getProvinceInfo(mapDataFolder, provinces, color2provID)
	fmt.Println("finding and classifying water provinces")
	getWater(provinces)
	fmt.Println("finding neighboring and nearby provinces")
	getNeighbors(provinces, mapDataFolder, color2provID)
	fmt.Println("writing provinces")
	writeProvinces(provinces, outDir, outName)
}

func writeProvinces(provinces map[string]*prov, outDir string, outName string) {
	err := os.MkdirAll(filepath.Dir(filepath.Join(outDir,outName)), 0755)
	outFile, err := os.Create(filepath.Join(outDir, outName))
	if err != nil {
		fmt.Println("Failed to create new file: " + filepath.Join(outDir, outName))
		time.Sleep(100)
		log.Fatal(err)
	}
	fmt.Println("created file at " + filepath.Join(outDir, outName))

	writeHeader(outFile)
	_, _ = outFile.WriteString("demd_initialize_sea_land_adjacency = {\n")
	_, _ = outFile.WriteString("\n")
	_, _ = outFile.WriteString("\t# Water Adjacencies\n\n")
	for id, province := range provinces {
		if !strings.Contains(province.ID, "#") && province.isMajorRiver {
			_, _ = outFile.WriteString("\tprovince:" + id + " = { # " + province.name + "\n")

			counter := 0
			for _, neighborID := range province.neighbors {
				if !provinces[neighborID].isSea && !provinces[neighborID].isMajorRiver && !provinces[neighborID].isLake {
					_, _ = outFile.WriteString("\t\tprovince:" + neighborID + " = { demd_add_disaster_county = { DISASTER = flood } } # " + provinces[neighborID].name + "\n")
					counter++
				}
			}
			if counter > 0 {
				_, _ = outFile.WriteString("\n")
				_, _ = outFile.WriteString("\t\tadd_to_global_variable_list = { name = demd_flood_provinces target = this }\n")
			}
			_, _ = outFile.WriteString("\t}\n")
		} else if !strings.Contains(province.ID, "#") && province.isSea {
			_, _ = outFile.WriteString("\tprovince:" + id + " = { # " + province.name + "\n")

			counter := 0
			for _, neighborID := range province.neighbors {
				if !provinces[neighborID].isSea && !provinces[neighborID].isMajorRiver && !provinces[neighborID].isLake {
					_, _ = outFile.WriteString("\t\tprovince:" + neighborID + " = { demd_add_disaster_county = { DISASTER = tsunami } } # " + provinces[neighborID].name + "\n")
					counter++
				}
			}
			if counter > 0 {
				_, _ = outFile.WriteString("\n")
				_, _ = outFile.WriteString("\t\tadd_to_global_variable_list = { name = demd_tsunami_provinces target = this }\n")
			}
			_, _ = outFile.WriteString("\t}\n")
		}
	}
	_, _ = outFile.WriteString("}")
}

func getProvinces(mapDataFolder string) (map[string]*prov, map[string]string) {
	// Open Province File
	thisFile, err := os.Open(filepath.Join(mapDataFolder, "definition.csv"))
	if err != nil {
		log.Fatal(err)
	}

	// Initialize scanner
	scanner := bufio.NewScanner(thisFile)

	provinces := make(map[string]*prov)
	color2provID := make(map[string]string)

	for scanner.Scan() {
		// get next line
		line := scanner.Text()
		fields := strings.Split(line, ";")
		if len(fields) > 3 {
			color := fields[1] + "." + fields[2] + "." + fields[3]

			var newProv prov
			newProv.ID = fields[0]
			newProv.name = fields[4]
			newProv.color = color
			newProv.pixels = []*pixel{}

			provinces[newProv.ID] = &newProv
			color2provID[color] = newProv.ID
		}
	}
	return provinces, color2provID
}

func getProvinceInfo(mapDataFolder string, provinces map[string]*prov, color2provID map[string]string) map[string]*prov {
	// Open Province File
	provinceFile, err := os.Open(filepath.Join(mapDataFolder, "provinces.png"))
	if err != nil {
		log.Fatal(err)
	}

	provinceMap, err := png.Decode(provinceFile)
	if err != nil {
		log.Fatal(err)
	}

	// Open River File
	riverFile, err := os.Open(filepath.Join(mapDataFolder, "rivers.png"))
	if err != nil {
		log.Fatal(err)
	}

	riverMap, err := png.Decode(riverFile)
	if err != nil {
		log.Fatal(err)
	}

	for y := provinceMap.Bounds().Min.Y; y < provinceMap.Bounds().Max.Y; y++ {
		for x := provinceMap.Bounds().Min.X; x < provinceMap.Bounds().Max.X; x++ {
			r, g, b, _ := provinceMap.At(x, y).RGBA()
			unifiedColor := rgbaToUnified(int(r / 0x101), int(g / 0x101) , int(b / 0x101))
			//fmt.Println(unifiedColor)
			provID := color2provID[unifiedColor]


			var pix pixel
			pix.x = float64(x)
			pix.y = float64(y)
			pix.color = unifiedColor
			pix.provID = provID

			// rivermap info
			rr, rg, rb, _ := riverMap.At(x, y).RGBA()
			unifiedColor = rgbaToUnified(int(rr / 0x101), int(rg / 0x101) , int(rb / 0x101))

			if unifiedColor == "255.255.255" {
				pix.class = "land"
			} else if unifiedColor == "255.0.128" {
				pix.class = "sea"
			} else {
				pix.class = "river"
			}

			if _, ok := provinces[provID]; ok {
				provinces[provID].pixels = append(provinces[provID].pixels, &pix)
			} else {
				fmt.Println("trying to add pixel to province \"" + provID + "\", which does not exist.\nThis error occurs when the pixel color was not found in map_data\\definition.csv.")
			}
		}
	}
	return provinces
}

func getNeighbors(provinces map[string]*prov, mapDataFolder string, color2provID map[string]string) {
	// Open Province File
	provinceFile, err := os.Open(filepath.Join(mapDataFolder, "provinces.png"))
	if err != nil {
		log.Fatal(err)
	}

	provinceMap, err := png.Decode(provinceFile)
	if err != nil {
		log.Fatal(err)
	}

	for _, province := range provinces {
		for _, pix := range province.pixels {
			r, g, b, _ := provinceMap.At(int(pix.x), int(pix.y)).RGBA()
			unifiedColor := rgbaToUnified(int(r/0x101), int(g/0x101), int(b/0x101))

			neighborPixels := [][]int{{int(pix.x) + 1, int(pix.y) + 1}, {int(pix.x) - 1, int(pix.y) + 1}, {int(pix.x) + 1, int(pix.y) - 1}, {int(pix.x) - 1, int(pix.y) - 1}}
			for _, neighbor := range neighborPixels {
				r, g, b, _ := provinceMap.At(neighbor[0], neighbor[1]).RGBA()
				neighborColor := rgbaToUnified(int(r/0x101), int(g/0x101), int(b/0x101))
				if neighborColor != unifiedColor {
					makeNeighbors(province, provinces[color2provID[neighborColor]])
				}
			}
		}
	}
}

func getWater(provinces map[string]*prov) {
	for _, province := range provinces {
		fractionLand := 0.0
		for _, pixel := range province.pixels {
			if pixel.class == "land" {
				fractionLand++
			}
		}
		fractionLand = fractionLand / float64(len(province.pixels))
		if fractionLand > 0.5 {
			province.isLand = true
		}
	}

	for _, province := range provinces {
		if !province.isLand {
			riverIdentifiers := []string{"river", "ilinalta", "honrich", "geir", "yorgrim"}
			lakeIdentifiers := []string{"lake", "lagoon", "pond", "geyser"}
			if nameIndicatesType(province.name, riverIdentifiers) {
				province.isMajorRiver = true
			} else if nameIndicatesType(province.name, lakeIdentifiers) {
				province.isLake = true
			} else {
				province.isSea = true
			}
		}
	}
}

func makeNeighbors(prov1 *prov, prov2 *prov) {
	if prov1 == nil || prov2 == nil {
		return
	}
	for _, v := range prov1.neighbors {
		if v == prov2.ID {
			return
		}
	}
	prov1.neighbors = append(prov1.neighbors, prov2.ID)
	prov2.neighbors = append(prov2.neighbors, prov1.ID)
}

func nameIndicatesType(name string, identifiers []string) bool {
	cleanedName := strings.ToLower(name)

	for _, identifier := range identifiers {
		if strings.Contains(cleanedName, identifier) {
			return true
		}
	}
	return false
}

type pixel struct {
	color string
	provID string
	elevation float64
	class string
	x float64
	y float64
}

func rgbaToUnified(r int, g int, b int) string {
	newr := float64(r)
	newg := float64(g)
	newb := float64(b)
	return strconv.Itoa(int(newr)) + "." + strconv.Itoa(int(newg)) + "." + strconv.Itoa(int(newb))
}

type prov struct {
	name      string
	x         float64
	y         float64

	color     string
	ID        string
	neighbors []string

	pixels    []*pixel

	isLand               bool

	isLake       bool
	isMajorRiver bool
	isSea        bool
}