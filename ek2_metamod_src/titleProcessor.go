package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

func getCleanedString(string string) string {
	cleanString := strings.ReplaceAll(string, "=", "")
	return cleanString
}


func titleAssignment(modDir string, sourceDir string) {

	landedTitlesFolder := filepath.Join(sourceDir, "common", "landed_titles")
	outDir := filepath.Join(modDir, "common", "scripted_effects")
	var titles []string

	fileInfo, err := ioutil.ReadDir(landedTitlesFolder)
	if err != nil {
		fmt.Println("failed to read directory: " + landedTitlesFolder)
		log.Fatal(err)
	}
	for _, file := range fileInfo {
		if !strings.Contains(file.Name(), ".info") {
			filePath := filepath.Join(landedTitlesFolder, file.Name())
			thisFile, err := os.Open(filePath)
			if err != nil {
				fmt.Println("Failed to open file: " + filePath)
				log.Fatal(err)
			}
			scanner := bufio.NewScanner(thisFile)
			for scanner.Scan() {
				// get next line
				line := scanner.Text()
				line = cleanLine(line)
				fields := strings.Fields(line)
				if len(fields) > 0 {
					identifier := strings.Split(fields[0], "_")[0]
					if identifier == "c" {
						titles = append(titles, getCleanedString(fields[0]))
					}
				}
			}
		}

	}

	outPath := filepath.Join(outDir,"demd_initializer.txt")
	outFile, err := os.Create(outPath)
	if err != nil {
		fmt.Println("Failed to create new file: " + outPath)
		time.Sleep(100)
		log.Fatal(err)
	}
	fmt.Println("created file at " + outPath)

	writeHeader(outFile)
	_, _ = outFile.WriteString("demd_initialize_province_variables = {\n")
	_, _ = outFile.WriteString("\n")

	counter := 1
	counterM := 1
	for _, county := range titles {
		// monthly split
		_, _ = outFile.WriteString("\t# add_to_global_variable_list = { name = eligible_counties_group_m_" + strconv.Itoa(counterM) + " target = title:" + county + " }\n")
		// annual split
		_, _ = outFile.WriteString("\tadd_to_global_variable_list = { name = eligible_counties_group_y_" + strconv.Itoa(counter) + " target = title:" + county + " }\n")
		counter++
		counterM++
		if counter > 365 {
			counter = 1
		}
		if counterM > 30 {
			counterM = 1
		}
	}
	_, _ = outFile.WriteString("}\n")
}


