package main

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"time"
)

func onActionPrinter(modDir string) {
	oaDir := filepath.Join(modDir, "common","on_action")
	oaFile := filepath.Join(oaDir, "demd_metamod_game_start_on_actions.txt")
	err := os.MkdirAll(filepath.Dir(oaDir), 0755)
	if err != nil {
		fmt.Println("Failed to create new directory: " + oaDir)
		time.Sleep(100)
		log.Fatal(err)
	}
	outFile, err := os.Create(oaFile)
	if err != nil {
		fmt.Println("Failed to create new file: " + oaFile)
		time.Sleep(100)
		log.Fatal(err)
	}
	writeHeader(outFile)

	for i := 1; i <= 365; i++ {
		_, err = outFile.WriteString("yearly_global_pulse = {\n")
		_, err = outFile.WriteString("\ton_actions = {\n")
		if i > 1 {
			_, err = outFile.WriteString("\t\tdelay = { days = " + strconv.Itoa(i-1) + " }\n")
		}
		_, err = outFile.WriteString("\t\tdemd_meta_population_" + strconv.Itoa(i) + "\n")
		_, err = outFile.WriteString("\t}\n")
		_, err = outFile.WriteString("}\n")
	}

	for i := 1; i <= 365; i++ {
		_, err = outFile.WriteString("demd_meta_population_" + strconv.Itoa(i) + " = {\n")
		_, err = outFile.WriteString("\teffect = {\n")
		_, err = outFile.WriteString("\t\tannualPulse = { list = eligible_counties_group_y_" + strconv.Itoa(i) + " }\n")
		_, err = outFile.WriteString("\t}\n")
		_, err = outFile.WriteString("}\n")
	}
}

