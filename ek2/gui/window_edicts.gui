﻿types Edicts  {
    type window_edicts = window {
        size = { 400 600 }
        position = { 580 15 }
        layer = middle
		visible = "[GetVariableSystem.Exists('window_edicts')]"
    
        using = Window_Background
        using = Window_Decoration
        using = Animation_ShowHide_Standard
        
        vbox = {
            using = Window_Margins
            restrictparent_min = yes
    
            header_pattern = {
                layoutpolicy_horizontal = expanding
    
                blockoverride "header_text"
                {
                    raw_text = "[demd_edicts|E]"
                }
    
                blockoverride "button_close"
                {
                    onclick = "[GetVariableSystem.Clear( 'window_edicts' )]"
                }
            }
			
			
    
			flowcontainer = {
				datacontext = "[Province.GetCounty.GetTitle]"
				direction = vertical
				parentanchor = top
				spacing = 35
				background = {
					visible = "[LessThanOrEqualTo_int32( CharacterSelectionList.GetTotalNumber, '(int32)0' )]"
					texture = "gfx/interface/illustrations/council/bg_council_steward.dds"

					frame = 2
					alpha = 0.4
					using = Mask_Rough_Edges

					modify_texture = {
						texture = "gfx/interface/component_masks/mask_fade_vertical.dds"
						blend_mode = alphamultiply
						mirror = vertical
					}
				}
				widget_county_amenity_right = { # Tax
					blockoverride "amenity_name" { text = "[Title.Custom('demd_tax_edict_get_modifier')]" }
					blockoverride "amenity_icon_texture" { texture = "gfx/interface/window_roco_grandeur/icon_amenity_lodgings.dds" } 
					blockoverride "amenity_level_int" { text = "[Title.MakeScope.Var('tax_setting').GetValue|0]" } 
					
					# LEVEL 0
					blockoverride "amenity_level_0_effects" { 
						onclick = "[GetScriptedGui('demd_set_tax_0').Execute( GuiScope.SetRoot( Title.MakeScope ).End ) ]"
						enabled = "[And(EqualTo_uint32( Title.GetHolder.GetID,GetPlayer.GetID),GetScriptedGui('demd_set_tax_0').IsValid( GuiScope.SetRoot( Title.MakeScope ).End))]"
					}
					blockoverride "amenity_level_0_visible_sunburst" { 
						visible = "[EqualTo_CFixedPoint( Title.MakeScope.Var('tax_setting').GetValue, '(CFixedPoint)0')]"
					}
					blockoverride "amenity_level_0_visible_dot" {
						visible = "[EqualTo_CFixedPoint( Title.MakeScope.Var('tax_setting').GetValue, '(CFixedPoint)0')]"
					}
					blockoverride "amenity_level_0_tooltip" { tooltip = DEMD_TAX_0_TOOLTIP }
					
					# LEVEL 1
					blockoverride "amenity_level_1_effects" { 
						onclick = "[GetScriptedGui('demd_set_tax_1').Execute( GuiScope.SetRoot( Title.MakeScope ).End ) ]"
						enabled = "[And(EqualTo_uint32( Title.GetHolder.GetID,GetPlayer.GetID),GetScriptedGui('demd_set_tax_1').IsValid( GuiScope.SetRoot( Title.MakeScope ).End))]"
					}
					blockoverride "amenity_level_1_visible_sunburst" { 
						visible = "[EqualTo_CFixedPoint( Title.MakeScope.Var('tax_setting').GetValue, '(CFixedPoint)1')]"
					}
					blockoverride "amenity_level_1_visible_dot" {
						visible = "[EqualTo_CFixedPoint( Title.MakeScope.Var('tax_setting').GetValue, '(CFixedPoint)1')]"
					}
					blockoverride "amenity_level_1_tooltip" { tooltip = DEMD_TAX_1_TOOLTIP }
					
					# LEVEL 2
					blockoverride "amenity_level_2_effects" { 
						onclick = "[GetScriptedGui('demd_set_tax_2').Execute( GuiScope.SetRoot( Title.MakeScope ).End ) ]"
						enabled = "[And(EqualTo_uint32( Title.GetHolder.GetID,GetPlayer.GetID),GetScriptedGui('demd_set_tax_2').IsValid( GuiScope.SetRoot( Title.MakeScope ).End))]"
					}
					blockoverride "amenity_level_2_visible_sunburst" { 
						visible = "[EqualTo_CFixedPoint( Title.MakeScope.Var('tax_setting').GetValue, '(CFixedPoint)2')]"
					}
					blockoverride "amenity_level_2_visible_dot" {
						visible = "[EqualTo_CFixedPoint( Title.MakeScope.Var('tax_setting').GetValue, '(CFixedPoint)2')]"
					}
					blockoverride "amenity_level_2_tooltip" { tooltip = DEMD_TAX_2_TOOLTIP }
					
					# LEVEL 3
					blockoverride "amenity_level_3_effects" { 
						onclick = "[GetScriptedGui('demd_set_tax_3').Execute( GuiScope.SetRoot( Title.MakeScope ).End ) ]"
						enabled = "[And(EqualTo_uint32( Title.GetHolder.GetID,GetPlayer.GetID),GetScriptedGui('demd_set_tax_3').IsValid( GuiScope.SetRoot( Title.MakeScope ).End))]"
					}
					blockoverride "amenity_level_3_visible_sunburst" { 
						visible = "[EqualTo_CFixedPoint( Title.MakeScope.Var('tax_setting').GetValue, '(CFixedPoint)3')]"
					}
					blockoverride "amenity_level_3_visible_dot" {
						visible = "[EqualTo_CFixedPoint( Title.MakeScope.Var('tax_setting').GetValue, '(CFixedPoint)3')]"
					}
					blockoverride "amenity_level_3_tooltip" { tooltip = DEMD_TAX_3_TOOLTIP }
					
					# LEVEL 4
					blockoverride "amenity_level_4_effects" { 
						onclick = "[GetScriptedGui('demd_set_tax_4').Execute( GuiScope.SetRoot( Title.MakeScope ).End ) ]"
						enabled = "[And(EqualTo_uint32( Title.GetHolder.GetID,GetPlayer.GetID),GetScriptedGui('demd_set_tax_4').IsValid( GuiScope.SetRoot( Title.MakeScope ).End))]"
					}
					blockoverride "amenity_level_4_visible_sunburst" { 
						visible = "[EqualTo_CFixedPoint( Title.MakeScope.Var('tax_setting').GetValue, '(CFixedPoint)4')]"
					}
					blockoverride "amenity_level_4_visible_dot" {
						visible = "[EqualTo_CFixedPoint( Title.MakeScope.Var('tax_setting').GetValue, '(CFixedPoint)4')]"
					}
					blockoverride "amenity_level_4_tooltip" { tooltip = DEMD_TAX_4_TOOLTIP }											
					
				}
				widget_county_amenity_right = { # Manpower
					blockoverride "amenity_name" { text = "[Title.Custom('demd_manpower_edict_get_modifier')]" }
					blockoverride "amenity_icon_texture" { texture = "gfx/interface/window_roco_grandeur/icon_amenity_lodgings.dds" } 
					blockoverride "amenity_level_int" { text = "[Title.MakeScope.Var('manpower_setting').GetValue|0]" } 
					
					# LEVEL 0
					blockoverride "amenity_level_0_effects" { 
						onclick = "[GetScriptedGui('demd_set_manpower_0').Execute( GuiScope.SetRoot( Title.MakeScope ).End ) ]"
						enabled = "[And(EqualTo_uint32( Title.GetHolder.GetID,GetPlayer.GetID),GetScriptedGui('demd_set_manpower_0').IsValid( GuiScope.SetRoot( Title.MakeScope ).End))]"
					}
					blockoverride "amenity_level_0_visible_sunburst" { 
						visible = "[EqualTo_CFixedPoint( Title.MakeScope.Var('manpower_setting').GetValue, '(CFixedPoint)0')]"
					}
					blockoverride "amenity_level_0_visible_dot" {
						visible = "[EqualTo_CFixedPoint( Title.MakeScope.Var('manpower_setting').GetValue, '(CFixedPoint)0')]"
					}
					blockoverride "amenity_level_0_tooltip" { tooltip = DEMD_MANPOWER_0_TOOLTIP }
					
					# LEVEL 1
					blockoverride "amenity_level_1_effects" { 
						onclick = "[GetScriptedGui('demd_set_manpower_1').Execute( GuiScope.SetRoot( Title.MakeScope ).End ) ]"
						enabled = "[And(EqualTo_uint32( Title.GetHolder.GetID,GetPlayer.GetID),GetScriptedGui('demd_set_manpower_1').IsValid( GuiScope.SetRoot( Title.MakeScope ).End))]"
					}
					blockoverride "amenity_level_1_visible_sunburst" { 
						visible = "[EqualTo_CFixedPoint( Title.MakeScope.Var('manpower_setting').GetValue, '(CFixedPoint)1')]"
					}
					blockoverride "amenity_level_1_visible_dot" {
						visible = "[EqualTo_CFixedPoint( Title.MakeScope.Var('manpower_setting').GetValue, '(CFixedPoint)1')]"
					}
					blockoverride "amenity_level_1_tooltip" { tooltip = DEMD_MANPOWER_1_TOOLTIP }
					
					# LEVEL 2
					blockoverride "amenity_level_2_effects" { 
						onclick = "[GetScriptedGui('demd_set_manpower_2').Execute( GuiScope.SetRoot( Title.MakeScope ).End ) ]"
						enabled = "[And(EqualTo_uint32( Title.GetHolder.GetID,GetPlayer.GetID),GetScriptedGui('demd_set_manpower_2').IsValid( GuiScope.SetRoot( Title.MakeScope ).End))]"
					}
					blockoverride "amenity_level_2_visible_sunburst" { 
						visible = "[EqualTo_CFixedPoint( Title.MakeScope.Var('manpower_setting').GetValue, '(CFixedPoint)2')]"
					}
					blockoverride "amenity_level_2_visible_dot" {
						visible = "[EqualTo_CFixedPoint( Title.MakeScope.Var('manpower_setting').GetValue, '(CFixedPoint)2')]"
					}
					blockoverride "amenity_level_2_tooltip" { tooltip = DEMD_MANPOWER_2_TOOLTIP }
					
					# LEVEL 3
					blockoverride "amenity_level_3_effects" { 
						onclick = "[GetScriptedGui('demd_set_manpower_3').Execute( GuiScope.SetRoot( Title.MakeScope ).End ) ]"
						enabled = "[And(EqualTo_uint32( Title.GetHolder.GetID,GetPlayer.GetID),GetScriptedGui('demd_set_manpower_3').IsValid( GuiScope.SetRoot( Title.MakeScope ).End))]"
					}
					blockoverride "amenity_level_3_visible_sunburst" { 
						visible = "[EqualTo_CFixedPoint( Title.MakeScope.Var('manpower_setting').GetValue, '(CFixedPoint)3')]"
					}
					blockoverride "amenity_level_3_visible_dot" {
						visible = "[EqualTo_CFixedPoint( Title.MakeScope.Var('manpower_setting').GetValue, '(CFixedPoint)3')]"
					}
					blockoverride "amenity_level_3_tooltip" { tooltip = DEMD_MANPOWER_3_TOOLTIP }
					
					# LEVEL 4
					blockoverride "amenity_level_4_effects" { 
						onclick = "[GetScriptedGui('demd_set_manpower_4').Execute( GuiScope.SetRoot( Title.MakeScope ).End ) ]"
						enabled = "[And(EqualTo_uint32( Title.GetHolder.GetID,GetPlayer.GetID),GetScriptedGui('demd_set_manpower_4').IsValid( GuiScope.SetRoot( Title.MakeScope ).End))]"
					}
					blockoverride "amenity_level_4_visible_sunburst" { 
						visible = "[EqualTo_CFixedPoint( Title.MakeScope.Var('manpower_setting').GetValue, '(CFixedPoint)4')]"
					}
					blockoverride "amenity_level_4_visible_dot" {
						visible = "[EqualTo_CFixedPoint( Title.MakeScope.Var('manpower_setting').GetValue, '(CFixedPoint)4')]"
					}
					blockoverride "amenity_level_4_tooltip" { tooltip = DEMD_MANPOWER_4_TOOLTIP }											
					
				}
			
				widget_county_amenity_right = { # public_order
					blockoverride "amenity_name" { text = "[Title.Custom('demd_public_order_edict_get_modifier')]" }
					blockoverride "amenity_icon_texture" { texture = "gfx/interface/window_roco_grandeur/icon_amenity_lodgings.dds" }
					blockoverride "amenity_level_int" { text = "[Title.MakeScope.Var('public_order_setting').GetValue|0]" } 
					
					# LEVEL 0
					blockoverride "amenity_level_0_effects" { 
						onclick = "[GetScriptedGui('demd_set_public_order_0').Execute( GuiScope.SetRoot( Title.MakeScope ).End ) ]"
						enabled = "[And(EqualTo_uint32( Title.GetHolder.GetID,GetPlayer.GetID),GetScriptedGui('demd_set_public_order_0').IsValid( GuiScope.SetRoot( Title.MakeScope ).End))]"
					}
					blockoverride "amenity_level_0_visible_sunburst" { 
						visible = "[EqualTo_CFixedPoint( Title.MakeScope.Var('public_order_setting').GetValue, '(CFixedPoint)0')]"
					}
					blockoverride "amenity_level_0_visible_dot" {
						visible = "[EqualTo_CFixedPoint( Title.MakeScope.Var('public_order_setting').GetValue, '(CFixedPoint)0')]"
					}
					blockoverride "amenity_level_0_tooltip" { tooltip = DEMD_PUBLIC_ORDER_0_TOOLTIP }
					
					# LEVEL 1
					blockoverride "amenity_level_1_effects" { 
						onclick = "[GetScriptedGui('demd_set_public_order_1').Execute( GuiScope.SetRoot( Title.MakeScope ).End ) ]"
						enabled = "[And(EqualTo_uint32( Title.GetHolder.GetID,GetPlayer.GetID),GetScriptedGui('demd_set_public_order_1').IsValid( GuiScope.SetRoot( Title.MakeScope ).End))]"
					}
					blockoverride "amenity_level_1_visible_sunburst" { 
						visible = "[EqualTo_CFixedPoint( Title.MakeScope.Var('public_order_setting').GetValue, '(CFixedPoint)1')]"
					}
					blockoverride "amenity_level_1_visible_dot" {
						visible = "[EqualTo_CFixedPoint( Title.MakeScope.Var('public_order_setting').GetValue, '(CFixedPoint)1')]"
					}
					blockoverride "amenity_level_1_tooltip" { tooltip = DEMD_PUBLIC_ORDER_1_TOOLTIP }
					
					# LEVEL 2
					blockoverride "amenity_level_2_effects" { 
						onclick = "[GetScriptedGui('demd_set_public_order_2').Execute( GuiScope.SetRoot( Title.MakeScope ).End ) ]"
						enabled = "[And(EqualTo_uint32( Title.GetHolder.GetID,GetPlayer.GetID),GetScriptedGui('demd_set_public_order_2').IsValid( GuiScope.SetRoot( Title.MakeScope ).End))]"
					}
					blockoverride "amenity_level_2_visible_sunburst" { 
						visible = "[EqualTo_CFixedPoint( Title.MakeScope.Var('public_order_setting').GetValue, '(CFixedPoint)2')]"
					}
					blockoverride "amenity_level_2_visible_dot" {
						visible = "[EqualTo_CFixedPoint( Title.MakeScope.Var('public_order_setting').GetValue, '(CFixedPoint)2')]"
					}
					blockoverride "amenity_level_2_tooltip" { tooltip = DEMD_PUBLIC_ORDER_2_TOOLTIP }
					
					# LEVEL 3
					blockoverride "amenity_level_3_effects" { 
						onclick = "[GetScriptedGui('demd_set_public_order_3').Execute( GuiScope.SetRoot( Title.MakeScope ).End ) ]"
						enabled = "[And(EqualTo_uint32( Title.GetHolder.GetID,GetPlayer.GetID),GetScriptedGui('demd_set_public_order_3').IsValid( GuiScope.SetRoot( Title.MakeScope ).End))]"
					}
					blockoverride "amenity_level_3_visible_sunburst" { 
						visible = "[EqualTo_CFixedPoint( Title.MakeScope.Var('public_order_setting').GetValue, '(CFixedPoint)3')]"
					}
					blockoverride "amenity_level_3_visible_dot" {
						visible = "[EqualTo_CFixedPoint( Title.MakeScope.Var('public_order_setting').GetValue, '(CFixedPoint)3')]"
					}
					blockoverride "amenity_level_3_tooltip" { tooltip = DEMD_PUBLIC_ORDER_3_TOOLTIP }
					
					# LEVEL 4
					blockoverride "amenity_level_4_effects" { 
						onclick = "[GetScriptedGui('demd_set_public_order_4').Execute( GuiScope.SetRoot( Title.MakeScope ).End ) ]"
						enabled = "[And(EqualTo_uint32( Title.GetHolder.GetID,GetPlayer.GetID),GetScriptedGui('demd_set_public_order_4').IsValid( GuiScope.SetRoot( Title.MakeScope ).End))]"
					}
					blockoverride "amenity_level_4_visible_sunburst" { 
						visible = "[EqualTo_CFixedPoint( Title.MakeScope.Var('public_order_setting').GetValue, '(CFixedPoint)4')]"
					}
					blockoverride "amenity_level_4_visible_dot" {
						visible = "[EqualTo_CFixedPoint( Title.MakeScope.Var('public_order_setting').GetValue, '(CFixedPoint)4')]"
					}
					blockoverride "amenity_level_4_tooltip" { tooltip = DEMD_PUBLIC_ORDER_4_TOOLTIP }											
					
				}
				widget_county_amenity_right = { # sanitation
					blockoverride "amenity_name" { text = "[Title.Custom('demd_sanitation_edict_get_modifier')]" }
					blockoverride "amenity_icon_texture" { texture = "gfx/interface/window_roco_grandeur/icon_amenity_servants.dds" }
					blockoverride "amenity_level_int" { text = "[Title.MakeScope.Var('sanitation_setting').GetValue|0]" } 
					
					# LEVEL 0
					blockoverride "amenity_level_0_effects" { 
						onclick = "[GetScriptedGui('demd_set_sanitation_0').Execute( GuiScope.SetRoot( Title.MakeScope ).End ) ]"
						enabled = "[And(EqualTo_uint32( Title.GetHolder.GetID,GetPlayer.GetID),GetScriptedGui('demd_set_sanitation_0').IsValid( GuiScope.SetRoot( Title.MakeScope ).End))]"
					}
					blockoverride "amenity_level_0_visible_sunburst" { 
						visible = "[EqualTo_CFixedPoint( Title.MakeScope.Var('sanitation_setting').GetValue, '(CFixedPoint)0')]"
					}
					blockoverride "amenity_level_0_visible_dot" {
						visible = "[EqualTo_CFixedPoint( Title.MakeScope.Var('sanitation_setting').GetValue, '(CFixedPoint)0')]"
					}
					blockoverride "amenity_level_0_tooltip" { tooltip = DEMD_SANITATION_0_TOOLTIP }
					
					# LEVEL 1
					blockoverride "amenity_level_1_effects" { 
						onclick = "[GetScriptedGui('demd_set_sanitation_1').Execute( GuiScope.SetRoot( Title.MakeScope ).End ) ]"
						enabled = "[And(EqualTo_uint32( Title.GetHolder.GetID,GetPlayer.GetID),GetScriptedGui('demd_set_sanitation_1').IsValid( GuiScope.SetRoot( Title.MakeScope ).End))]"
					}
					blockoverride "amenity_level_1_visible_sunburst" { 
						visible = "[EqualTo_CFixedPoint( Title.MakeScope.Var('sanitation_setting').GetValue, '(CFixedPoint)1')]"
					}
					blockoverride "amenity_level_1_visible_dot" {
						visible = "[EqualTo_CFixedPoint( Title.MakeScope.Var('sanitation_setting').GetValue, '(CFixedPoint)1')]"
					}
					blockoverride "amenity_level_1_tooltip" { tooltip = DEMD_SANITATION_1_TOOLTIP }
					
					# LEVEL 2
					blockoverride "amenity_level_2_effects" { 
						onclick = "[GetScriptedGui('demd_set_sanitation_2').Execute( GuiScope.SetRoot( Title.MakeScope ).End ) ]"
						enabled = "[And(EqualTo_uint32( Title.GetHolder.GetID,GetPlayer.GetID),GetScriptedGui('demd_set_sanitation_2').IsValid( GuiScope.SetRoot( Title.MakeScope ).End))]"
					}
					blockoverride "amenity_level_2_visible_sunburst" { 
						visible = "[EqualTo_CFixedPoint( Title.MakeScope.Var('sanitation_setting').GetValue, '(CFixedPoint)2')]"
					}
					blockoverride "amenity_level_2_visible_dot" {
						visible = "[EqualTo_CFixedPoint( Title.MakeScope.Var('sanitation_setting').GetValue, '(CFixedPoint)2')]"
					}
					blockoverride "amenity_level_2_tooltip" { tooltip = DEMD_SANITATION_2_TOOLTIP }
					
					# LEVEL 3
					blockoverride "amenity_level_3_effects" { 
						onclick = "[GetScriptedGui('demd_set_sanitation_3').Execute( GuiScope.SetRoot( Title.MakeScope ).End ) ]"
						enabled = "[And(EqualTo_uint32( Title.GetHolder.GetID,GetPlayer.GetID),GetScriptedGui('demd_set_sanitation_3').IsValid( GuiScope.SetRoot( Title.MakeScope ).End))]"
					}
					blockoverride "amenity_level_3_visible_sunburst" { 
						visible = "[EqualTo_CFixedPoint( Title.MakeScope.Var('sanitation_setting').GetValue, '(CFixedPoint)3')]"
					}
					blockoverride "amenity_level_3_visible_dot" {
						visible = "[EqualTo_CFixedPoint( Title.MakeScope.Var('sanitation_setting').GetValue, '(CFixedPoint)3')]"
					}
					blockoverride "amenity_level_3_tooltip" { tooltip = DEMD_SANITATION_3_TOOLTIP }
					
					# LEVEL 4
					blockoverride "amenity_level_4_effects" { 
						onclick = "[GetScriptedGui('demd_set_sanitation_4').Execute( GuiScope.SetRoot( Title.MakeScope ).End ) ]"
						enabled = "[And(EqualTo_uint32( Title.GetHolder.GetID,GetPlayer.GetID),GetScriptedGui('demd_set_sanitation_4').IsValid( GuiScope.SetRoot( Title.MakeScope ).End))]"
					}
					blockoverride "amenity_level_4_visible_sunburst" { 
						visible = "[EqualTo_CFixedPoint( Title.MakeScope.Var('sanitation_setting').GetValue, '(CFixedPoint)4')]"
					}
					blockoverride "amenity_level_4_visible_dot" {
						visible = "[EqualTo_CFixedPoint( Title.MakeScope.Var('sanitation_setting').GetValue, '(CFixedPoint)4')]"
					}
					blockoverride "amenity_level_4_tooltip" { tooltip = DEMD_SANITATION_4_TOOLTIP }											
					
				}
			}
        }
    }

}