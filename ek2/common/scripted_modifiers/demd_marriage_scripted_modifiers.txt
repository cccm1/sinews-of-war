﻿
demd_marriage_scripted_modifiers = {

	modifier = {
		add = -200
		has_game_rule = demd_less_frequent_intermarriage_on
		scope:secondary_actor.culture = {
			NOT = { has_same_heritage_family_as = { TARGET = scope:secondary_recipient.culture } }
		}
		desc = MARRIAGE_DIFFERENT_HERITAGE_FAMILY_REASON
	}

	modifier = {
		add = {
			value = -100
			if = {
				limit = {
					scope:secondary_actor = {
						culture = { has_cultural_parameter = dislikes_marrying_outside_of_culture }
						NOT = { culture = { has_same_culture_heritage = scope:secondary_recipient.culture } }
						NOR = {
							scope:secondary_actor.culture = { has_innovation = innovation_lingua_tamrielis }
							scope:secondary_recipient.culture = { has_innovation = innovation_lingua_tamrielis }
						}
					}
				}
				add = -100
			}
		}
		has_game_rule = demd_less_frequent_intermarriage_on
		scope:secondary_actor.culture = {
			has_same_heritage_family_as = { TARGET = scope:secondary_recipient.culture }
			NOT = { has_same_heritage_group_as = { TARGET = scope:secondary_recipient.culture } }
		}
		desc = MARRIAGE_DIFFERENT_HERITAGE_GROUP_REASON
	}

	modifier = {
		add = -50
		has_game_rule = demd_less_frequent_intermarriage_on
		scope:secondary_actor.culture = {
			has_same_heritage_group_as = { TARGET = scope:secondary_recipient.culture }
			NOT = { has_same_culture_heritage = scope:secondary_recipient.culture }
		}
		desc = MARRIAGE_DIFFERENT_HERITAGE_REASON
	}
}

demd_seduction_scripted_modifiers = {

	modifier = {
		add = -100
		has_game_rule = demd_less_frequent_intermarriage_on
		scope:owner.culture = {
			NOT = { has_same_heritage_family_as = { TARGET = scope:target.culture } }
		}
		desc = MARRIAGE_DIFFERENT_HERITAGE_FAMILY_REASON
	}

	modifier = {
		add = -50
		has_game_rule = demd_less_frequent_intermarriage_on
		scope:owner.culture = {
			has_same_heritage_family_as = { TARGET = scope:target.culture }
			NOT = { has_same_heritage_group_as = { TARGET = scope:target.culture } }
		}
		desc = MARRIAGE_DIFFERENT_HERITAGE_GROUP_REASON
	}

	modifier = {
		add = -25
		has_game_rule = demd_less_frequent_intermarriage_on
		scope:owner.culture = {
			has_same_heritage_group_as = { TARGET = scope:target.culture }
			NOT = { has_same_culture_heritage = scope:target.culture }
		}
		desc = MARRIAGE_DIFFERENT_HERITAGE_REASON
	}
	
	modifier = {
		add = -10
		has_game_rule = demd_less_frequent_intermarriage_on
		scope:owner = {
			culture = { has_same_culture_heritage = scope:target.culture }
			NOT = { has_same_culture_as = scope:target }
		}
		desc = MARRIAGE_DIFFERENT_CULTURE_REASON
	}
	modifier = {
		add = -40
		desc = YOU_DONT_SPEAK_THE_LANGUAGE
		has_game_rule = demd_less_frequent_intermarriage_on
		scope:owner = {
			NOT = { knows_language_of_culture = scope:target.culture }
			NOR = {
				scope:owner.culture = { has_innovation = innovation_lingua_tamrielis }
				scope:target.culture = { has_innovation = innovation_lingua_tamrielis }
			}
		}
	}
	modifier = {
		add = -20
		desc = YOU_DONT_SPEAK_THE_LANGUAGE
		has_game_rule = demd_less_frequent_intermarriage_on
		scope:owner = {
			NOT = { knows_language_of_culture = scope:target.culture }
			OR = {
				scope:owner.culture = { has_innovation = innovation_lingua_tamrielis }
				scope:target.culture = { has_innovation = innovation_lingua_tamrielis }
			}
		}
	}
}