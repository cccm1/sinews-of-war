﻿#Scripted Modifiers used for war

ai_occupy_modifier = {
	ai_value_modifier = {
		ai_honor = 0.25
		ai_compassion = 0.5
	}	
	modifier = {	
		add = -10
		culture = { cultural_acceptance = { target = scope:county.culture value < 30 } }
	}
	modifier = {	
		add = -10
		culture = { cultural_acceptance = { target = scope:county.culture value < 20 } }
	}
	modifier = {	
		add = -10
		culture = { cultural_acceptance = { target = scope:county.culture value > 10 } }
	}
	modifier = {	
		add = -20
		faith = { faith_hostility_level = { target = scope:county.faith value = 3 } } 
	}
}

ai_sack_modifier = {
	ai_value_modifier = {
		ai_honor = -0.25
		ai_compassion = -0.5
	}
	modifier = {
		culture = { has_cultural_parameter = ai_sacks_more }
		factor = 5
	}
	modifier = {	
		factor = 0
		NOT = { culture = { has_cultural_parameter = ai_sacks_more } }
		NAND = {
			culture = { cultural_acceptance = { target = scope:county.culture value < 40 } }
			faith = { faith_hostility_level = { target = scope:county.faith value > 1 } }
		}
	}
}

ai_raze_modifier = {
	ai_value_modifier = {
		ai_honor = -0.5
		ai_compassion = -1
	}
	modifier = {	
		factor = 0
		NAND = {
			culture = { cultural_acceptance = { target = scope:county.culture value < 25 } }
			faith = { faith_hostility_level = { target = scope:county.faith value > 2 } }
		}
	}	
}

ai_sack_acceptance_modifier = {
	ai_value_modifier = {
		ai_energy = -0.5	
		ai_vengefulness = -0.5	
	}
}


ai_sack_vengeance_modifier = {
	ai_value_modifier = {	
		ai_vengefulness = 0.5
		ai_energy = 0.5
	}
}

ai_sack_rebuilding_modifier = {
	ai_value_modifier = {
		ai_compassion = 0.5
		ai_greed = -0.5
	}
}