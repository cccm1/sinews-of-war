﻿demd_eligible_for_holding_conversion = {
	title_province = {
		trigger_if = {
			limit = { has_holding_type = $TARGET_TYPE$ }
			custom_description = { 
				text = demd_type_is_target_desc
				always = no
			}
		}
	}
	trigger_if = {
		limit = { tier < tier_county }
		title_province = {			
			trigger_if = {
				limit = { has_holding_type = castle_holding }
				trigger_if = {
					limit = { demd_holding_isnt_duplicate = { TYPE = castle } }
					custom_description = { 
						text = demd_castle_holding_isnt_duplicate_desc
						always = no
					}
				}
				
			}
			trigger_else_if = {
				limit = { has_holding_type = city_holding }
				trigger_if = {
					limit = { demd_holding_isnt_duplicate = { TYPE = city } }
					custom_description = { 
						text = demd_city_holding_isnt_duplicate_desc
						always = no
					}
				}		
			}
			trigger_else_if = {
				limit = { has_holding_type = church_holding }
				trigger_if = {
					limit = { demd_holding_isnt_duplicate = { TYPE = church } }
					custom_description = { 
						text = demd_church_holding_isnt_duplicate_desc
						always = no
					}
				}		
			}
			trigger_else = { 
				custom_description = { 
					text = demd_holding_isnt_valid_type_desc
					always = no
				}
			}
		}
	}
}

demd_holding_isnt_duplicate = {
	county = {
		any_county_province = {
			count < 2
			has_holding_type = $TYPE$_holding
		}
	}
}

demd_bs_bruma_music_trigger = {
	OR = {
		has_game_rule = demd_bs_bruma_music_always
		AND = {
			has_game_rule = demd_bs_bruma_music_provincial
			OR = {
				location = { geographical_region = mundus_tamriel_cyrodiil }
				culture = { has_cultural_pillar = heritage_cyrodiilic }
				culture = { 
					any_parent_culture_or_above = {
						this = culture:cyro_nord
					}
				}
			}
		}
		AND = {
			has_game_rule = demd_bs_bruma_music_regional
			OR = {
				location = { geographical_region = mundus_tamriel_cyrodiil_jerall_mountains }
				culture = { 
					any_parent_culture_or_above = {
						OR = {
							this = culture:cyro_nord
						}
					}
				}
			}
		}
	}
}

demd_bs_colovia_music_trigger = {
	OR = {
		has_game_rule = demd_bs_colovia_music_always
		AND = {
			has_game_rule = demd_bs_colovia_music_provincial
			OR = {
				location = { geographical_region = mundus_tamriel_cyrodiil }
				culture = { has_cultural_pillar = heritage_cyrodiilic }
			}
		}
		AND = {
			has_game_rule = demd_bs_colovia_music_regional
			OR = {
				location = { geographical_region = mundus_tamriel_cyrodiil_colovia }
				culture = { 
					any_parent_culture_or_above = {
						OR = {
							this = culture:colovian
						}
					}
				}
			}
		}
	}
}

demd_mmr_parameter_cb_trigger = {
	scope:attacker = {
		culture = {
			NAND = {
				has_cultural_parameter = no_same_heritage_mmr
				has_same_culture_heritage = scope:defender.culture
			}
		}
		faith = {
			NAND = {
				has_doctrine_parameter = no_intrafaith_mmr
				this = scope:defender.faith
			}
		}
	}
}