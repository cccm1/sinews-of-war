﻿### EK EDIT: Repository for EK Mer Core Traditions

##############################
# CORE CULTURAL TRADITIONS #
##############################


### Altmer Traditions

tradition_ceremoniarchy = { #Aedric Lineages
	category = regional

	layers = {
		0 = diplo
		1 = western
		4 = crown.dds
	}
	
	can_pick = {
		custom_tooltip = {
			text = tradition_ceremoniarchy_trigger_desc
			NOR = { 
				has_cultural_tradition = tradition_kings_in_exile
				has_cultural_tradition = tradition_remnant_kings
				has_cultural_tradition = tradition_hereditary_hierarchy
				has_cultural_tradition = tradition_heartland_high_elves
			}	
			OR = {
				has_cultural_pillar = heritage_aldmeri
				has_cultural_pillar = heritage_ayleid
			}
			scope:character.faith = {
				has_doctrine = tenet_legalism
				has_doctrine = doctrine_pantheon_aedra
			}
		}
	}
	
	parameters = {
		poorly_educated_leaders_greatly_distrusted_ceremoniarchy = yes
		unlocks_praxis_government = yes
		unlocks_feudal_government = yes
		renown_from_temple_construction = yes
		dislikes_marrying_outside_of_culture = yes
		dislikes_marrying_outside_of_heritage = yes
		ai_doesnt_marry_outside_culture = yes
		#Disinherit/Denounce renown cost scales based on their "purity", aka congenital traits, Impure, sins/virtues. AI more likely do disinherit/denounce "impure" relatives
		dynastic_interactions_purity = yes
		dynastic_interactions_more_likely = yes
		#Disinherited relatives don't wanna stick around
		disinherited_wanderers = yes
		#can_appoint_court_chirurgeon = yes #not made yet
	}
	character_modifier = {
		tyranny_loss_mult = -0.75
		piety_level_impact_mult = 0.5
		prestige_level_impact_mult = -0.5
	}
	
	cost = {
		prestige = {
			add = {
				value = tradition_base_cost
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
			if = {
				limit = {
					NOR = {
						any_in_list = { list = traits this = flag:ethos_courtly }
						any_in_list = { list = traits this = flag:ethos_spiritual }
					}
				}
				add = {
					value = tradition_incompatible_ethos_penalty
					desc = not_courtly_or_spiritual_desc
				}
			}
			if = {
				limit = {
					NOT = {
						scope:character = {
							primary_title.tier >= tier_kingdom
							any_vassal = {
								count >= 10
								primary_title.tier >= tier_county
								### EK NOTE: Again, we check if you have a Feudal or Autocratic gov
								OR = {
									has_government = feudal_government
									has_government = autocracy_government
									has_government = praxis_government
								}
							}
						}
					}
				}
				add = {
					value = tradition_unfulfilled_criteria_penalty
					desc = king_with_vassals_desc
				}
			}
			
			multiply = tradition_replacement_cost_if_relevant
		}
	}
	
	ai_will_do = {
		value = 100
		if = {
			limit = {
				NOT = {
					scope:character.primary_title.tier >= tier_kingdom
				}
			}
			multiply = 0
		}
	}
}

tradition_prescriptivism = {
	category = regional

	layers = {
		0 = learning
		1 = western
		4 = ceremony.dds
	}

	can_pick = {
		custom_tooltip = {
			text = tradition_prescriptivism_trigger_desc
			NOT = { has_cultural_tradition = tradition_ruling_caste } #otherwise they stack in a bad way
			has_cultural_pillar = heritage_aldmeri
		}
	}	

	parameters = {
		diplomacy_valued = yes
		learned_diplomats = yes
		learning_education_worse_outcomes = yes
		better_arcane_education = yes
		dont_want_foreign_languages = yes
		expensive_convert_to_local_culture = yes
		peasant_and_populist_factions_less_common = yes
	
	}
	culture_modifier = {
		cultural_acceptance_gain_mult = -0.25
	}
	character_modifier = {
		stress_gain_mult = -0.1
		cultural_head_fascination_mult = -0.1
		dread_decay_mult = -0.3
	}
	cost = {
		prestige = {
			add = {
				value = tradition_base_cost
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
			if = {
				limit = {
					NOR = {
						any_in_list = { list = traits this = flag:ethos_courtly }
						any_in_list = { list = traits this = flag:ethos_spiritual }
					}
				}
				add = {
					value = tradition_incompatible_ethos_penalty
					desc = not_courtly_or_spiritual_desc
				}
			}
			multiply = tradition_replacement_cost_if_relevant
		}
	}
	
	ai_will_do = {
		value = 100
		if = {
			limit = {
				NOR = {
					any_in_list = { list = traits this = flag:ethos_courtly }
					any_in_list = { list = traits this = flag:ethos_spiritual }
				}
			}
			multiply = 0
		}
	}
}

### Ayleid Traditions

tradition_remnant_kings = {
	category = regional

	layers = {
		0 = martial
		1 = western
		4 = crown.dds
	}

	can_pick = {
		custom_tooltip = {
			text = tradition_remnant_kings_trigger_desc
			NOR = { 
				has_cultural_tradition = tradition_kings_in_exile
				has_cultural_tradition = tradition_ceremoniarchy
				has_cultural_tradition = tradition_heartland_high_elves
			}	
			has_cultural_pillar = heritage_ayleid
		}
	}
	
	parameters = {
		#callous prestige
		callousness_valued = yes
		vengeful_trait_more_common = yes
		killing_rivals_is_pog = yes
		holy_warrior_trait_more_common = yes
		#holy warrior trait gives piety
		holy_warrior_piety = yes
	}
	culture_modifier = {
		cultural_acceptance_gain_mult = -0.25
	}
	character_modifier = {
		intrigue_per_prestige_level = 1
		monthly_prestige_gain_mult = -0.2
		same_culture_opinion = 5
	}

	cost = {
		prestige = {
			add = {
				value = tradition_base_cost
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
			if = {
				limit = {
					NOR = {
						any_in_list = { list = traits this = flag:ethos_bellicose }
						any_in_list = { list = traits this = flag:ethos_spiritual }
						any_in_list = { list = traits this = flag:ethos_stoic }
					}
				}
				add = {
					value = tradition_incompatible_ethos_penalty
					desc = not_bellicose_spiritual_or_stoic_desc
				}
			}

			multiply = tradition_replacement_cost_if_relevant
		}
	}
	
	ai_will_do = {
		value = 100
		if = {
			limit = {
				NOR = {
					any_in_list = { list = traits this = flag:ethos_spiritual }
					any_in_list = { list = traits this = flag:ethos_bellicose }
					any_in_list = { list = traits this = flag:ethos_stoic }
				}
			}
			multiply = 0.5
		}
	}
}

tradition_kings_in_exile = {
	category = regional

	layers = {
		0 = martial
		1 = western
		4 = crown.dds
	}

	can_pick = {
		custom_tooltip = {
			text = tradition_kings_in_exile_trigger_desc
			NOR = { 
				has_cultural_tradition = tradition_heartland_high_elves
				has_cultural_tradition = tradition_remnant_kings
				has_cultural_tradition = tradition_ceremoniarchy
			}	
			has_cultural_pillar = heritage_ayleid
		}
	}	

	parameters = {
		weak_rulers_contest_rule = yes
		martial_education_better_outcomes = yes
		intrigue_education_worse_outcomes = yes
		poorly_educated_leaders_distrusted = yes
		humble_trait_more_common = yes
		#zealous gives +1 learning
		zealous_bonuses = yes
		godliness_valued = yes
		dislikes_marrying_outside_of_culture = yes
		dislikes_marrying_outside_of_heritage = yes
		ai_doesnt_marry_outside_culture = yes
	}
	culture_modifier = {
		cultural_acceptance_gain_mult = 0.10
	}

	character_modifier = {
		martial_per_prestige_level = 1
		same_culture_opinion = 5
	}

	cost = {
		prestige = {
			add = {
				value = tradition_base_cost
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
			if = {
				limit = {
					NOR = {
						any_in_list = { list = traits this = flag:ethos_bellicose }
						any_in_list = { list = traits this = flag:ethos_spiritual }
						any_in_list = { list = traits this = flag:ethos_stoic }
					}
				}
				add = {
					value = tradition_incompatible_ethos_penalty
					desc = not_bellicose_spiritual_or_stoic_desc
				}
			}

			multiply = tradition_replacement_cost_if_relevant
		}
	}
	
	ai_will_do = {
		value = 100
		if = {
			limit = {
				NOR = {
					any_in_list = { list = traits this = flag:ethos_spiritual }
					any_in_list = { list = traits this = flag:ethos_bellicose }
					any_in_list = { list = traits this = flag:ethos_stoic }
				}
			}
			multiply = 0.5
		}
	}
}

tradition_heartland_high_elves = {
	category = regional

	layers = {
		0 = martial
		1 = western
		4 = crown.dds
	}

	can_pick = {
		custom_tooltip = {
			text = tradition_heartland_high_elves_trigger_desc
			NOR = { 
				has_cultural_tradition = tradition_kings_in_exile
				has_cultural_tradition = tradition_remnant_kings
				has_cultural_tradition = tradition_ceremoniarchy
			}	
			has_cultural_pillar = heritage_ayleid
		}
	}	

	parameters = {
		weak_rulers_contest_rule = yes
		poorly_educated_leaders_distrusted = yes
		#more likely to convert between Aedra and Daedra faiths
		schismatic_faith = yes
		peasant_and_populist_factions_less_common = yes
		dislikes_marrying_outside_of_culture = yes
		dislikes_marrying_outside_of_heritage = yes
		ai_doesnt_marry_outside_culture = yes
	}
	culture_modifier = {
		cultural_acceptance_gain_mult = -0.25
	}

	character_modifier = {
		vassal_opinion = -10
		dread_per_tyranny_add = 0.3
		dread_decay_mult = -0.3
		revolting_siege_morale_loss_mult = 0.25
	}

	cost = {
		prestige = {
			add = {
				value = tradition_base_cost
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
			if = {
				limit = {
					NOR = {
						any_in_list = { list = traits this = flag:ethos_bellicose }
						any_in_list = { list = traits this = flag:ethos_courtly }
						any_in_list = { list = traits this = flag:ethos_stoic }
					}
				}
				add = {
					value = tradition_incompatible_ethos_penalty
					desc = not_bellicose_stoic_or_courtly_desc
				}
			}

			multiply = tradition_replacement_cost_if_relevant
		}
	}
	
	ai_will_do = {
		value = 100
		if = {
			limit = {
				NOR = {
					any_in_list = { list = traits this = flag:ethos_courtly }
					any_in_list = { list = traits this = flag:ethos_bellicose }
					any_in_list = { list = traits this = flag:ethos_stoic }
				}
			}
			multiply = 0.5
		}
	}

}

### Bosmer Traditions

tradition_bosmer_core = {
	category = regional

	layers = {
		0 = martial
		1 = mena
		4 = bow.dds
	}
	
	#Replaces Jungle Warriors
	is_shown = {
		always = yes
	}
	can_pick = {
		custom_tooltip = {
			text = tradition_bosmer_core_trigger_desc
			NOT = { has_cultural_tradition = tradition_jungle_warriors }
			has_cultural_pillar = heritage_bosmeri
		}

		culture_not_pacifistic_trigger = yes
		custom_tooltip = {
			text = cannot_have_tradition_vegetarianism
			NOT = { any_in_list = { list = traits this = flag:tradition_vegetarianism } }
		}
	}
	
	parameters = {
		unlock_maa_bosmer_stalkers = yes
		more_frequent_hunts = yes
		hunting_traits_more_valued = yes
		can_recruit_prisoners_easily = yes
		jungle_trait_bonuses = yes
		jungle_stalker_trait_more_common = yes
		hardwork_valued = yes
	}

	province_modifier = {
		valenwood_tax_mult = 0.15
		valenwood_hills_tax_mult = 0.10
		valenwood_levy_size = 0.10 #EK EDIT: added a small bonus for regular Valenwood terrain but not hills
		valenwood_supply_limit_mult = 0.2
	}


	character_modifier = {
		archers_max_size_add = 3
		heavy_infantry_max_size_add = -2
		pikemen_max_size_add = -2
	}
	
	cost = {
		prestige = {
			add = {
				value = tradition_base_cost
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
			if = {
				limit = {
					NOR = {
						any_in_list = { list = traits this = flag:ethos_communal }
						any_in_list = { list = traits this = flag:ethos_egalitarian }
					}
				}
				add = {
					value = tradition_incompatible_ethos_penalty
					desc = not_communal_or_egalitarian_desc
				}
			}
			if = {
				limit = {
					NOT = {
						any_culture_county = {
							percent >= 0.3
							any_county_province = {
								terrain = jungle
							}
						}
					}
				}
				add = {
					value = tradition_unfulfilled_criteria_penalty
					desc = jungle_percentage_desc
				}
			}
			
			multiply = tradition_replacement_cost_if_relevant
		}
	}

	ai_will_do = {
		value = 100
		if = {
			limit = {
				NOT = {
					scope:character = {
						any_sub_realm_county = {
							culture = scope:character.culture
							any_county_province = {
								terrain = jungle
							}
						}
					}
				}
			}
			multiply = 0
		}
	}
}

### Chimer Traditions

tradition_velothi_exodus = {
	category = regional

	layers = {
		0 = learning
		1 = mediterranean
		4 = speech.dds
	}

	can_pick = {
		custom_tooltip = {
			text = tradition_chimeri_trigger_desc
			has_cultural_pillar = heritage_chimeri
		}
		NOT = { has_cultural_tradition = tradition_mendicant_mystics }
	}

	parameters = {
		can_recruit_organization_specialist = yes
		organizer_trait_better = yes
		bonuses_from_theologian_trait = yes
		zealous_trait_more_common = yes
		mystic_trait_gives_bonuses = yes
		mystic_trait_more_common = yes
		mendicant_mystics_may_appear = yes
		peasant_mystic_revolts_more_common = yes
	}

	cost = {
		prestige = {
			add = {
				value = tradition_base_cost
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
			if = {
				limit = {
					NOR = {
						any_in_list = { list = traits this = flag:ethos_communal }
						any_in_list = { list = traits this = flag:ethos_egalitarian }
						any_in_list = { list = traits this = flag:ethos_stoic }
					}
				}
				add = {
					value = tradition_incompatible_ethos_penalty
					desc = not_communal_egalitarian_or_stoic_desc
				}
			}
			
			multiply = tradition_replacement_cost_if_relevant
		}
	}

	ai_will_do = {
		value = 100
		if = {
			limit = {
				NOR = {
					any_in_list = { list = traits this = flag:ethos_communal }
					any_in_list = { list = traits this = flag:ethos_egalitarian }
					any_in_list = { list = traits this = flag:ethos_stoic }
				}
			}
			multiply = 0.5
		}
	}
}

### Dunmer Traditions

tradition_great_houses = { #Common to all Great House cultures
	category = regional

	layers = {
		0 = diplo
		1 = mediterranean
		4 = king.dds
	}

	can_pick = {
		custom_tooltip = {
			text = tradition_great_houses_trigger_desc
			NOT = { has_cultural_tradition = tradition_red_mountain_might }
			has_cultural_pillar = heritage_dunmeri
		}
	}
	parameters = {
		### EK EDIT: Unlocks the Duel for a Title interaction
		unlock_duel_for_title = yes
		#from tribal unity
		bonuses_from_patriarch_matriarch_trait = yes
		landing_house_members_give_prestige = yes
		penalty_for_revoking_titles_from_house_members = yes
		dislikes_marrying_outside_of_culture = yes #to hold us over until this can be handled by GHC
		dislikes_marrying_outside_of_heritage = yes #to hold us over until this can be handled by GHC
		ai_doesnt_marry_outside_culture = yes #to hold us over until this can be handled by GHC
		unlock_maa_dunmer_oathmen = yes
		dunmeri_elective_enabled = yes
	}

	character_modifier = {
		ai_war_chance = -0.25 #to hold us over until this can be handled by GHC
	}


	cost = {
		prestige = {
			add = {
				value = tradition_base_cost
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
			multiply = tradition_replacement_cost_if_relevant
		}
	}
}

tradition_bulwark_of_the_west = { #Gah-Julani/Redoran
	category = regional

	layers = {
		0 = martial
		1 = mediterranean
		4 = shield.dds
	}

	can_pick = {
		custom_tooltip = {
			text = tradition_bulwark_of_the_west_trigger_desc
			OR = {
				this = culture:gah_julan
				any_parent_culture_or_above = {
					this = culture:gah_julan
				}
			}
		}
	}
	
	parameters = {
		poorly_educated_martial_leaders_distrusted = yes
		bossy_trait_more_common = yes
		curious_trait_less_common = yes
		courage_valued = yes
		greedy_trait_looked_down_upon = yes
		can_end_defensive_wars_earlier = yes
	}

	character_modifier = {
		men_at_arms_recruitment_cost = 0.25
		martial_per_piety_level = 1
		controlled_province_advantage = 5
	}
	
	cost = {
		prestige = {
			add = {
				value = tradition_base_cost
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
			if = {
				limit = {
					NOR = {
						any_in_list = { list = traits this = flag:ethos_stoic }
					}
				}
				add = {
					value = tradition_incompatible_ethos_penalty
					desc = not_stoic_desc
				}
			}
			
			multiply = tradition_replacement_cost_if_relevant
		}
	}
}

tradition_ordination_of_war = { #Chuzei/Indoril
	category = regional

	layers = {
		0 = martial
		1 = mediterranean
		4 = fight.dds
	}

	can_pick = {
		custom_tooltip = {
			text = tradition_ordination_of_war_trigger_desc
			OR = {
				this = culture:chuzei
				any_parent_culture_or_above = {
					this = culture:chuzei
				}
			}
		}
	}
	
	parameters = {
		culture_clergy_can_fight = yes
		zealous_trait_more_common = yes
		monastic_expectations = yes
		#prestige bonus to zealous, prestige malus to cynical
		ordination_of_war_trait_values = yes
		holy_warrior_trait_more_common = yes
		prestige_from_temple_construction = yes
	}

	character_modifier = {
		prowess_per_piety_level = 2
		tolerance_advantage_mod = 5
	}
	
	cost = {
		prestige = {
			add = {
				value = tradition_base_cost
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
			if = {
				limit = {
					NOR = {
						any_in_list = { list = traits this = flag:ethos_spiritual }
					}
				}
				add = {
					value = tradition_incompatible_ethos_penalty
					desc = not_spiritual_desc
				}
			}
			
			multiply = tradition_replacement_cost_if_relevant
		}
	}
}

tradition_parliament_of_bugs = { #Chi-Adduni/Telvanni
	category = regional

	layers = {
		0 = steward
		1 = mediterranean
		4 = temple.dds
	}

	can_pick = {
		custom_tooltip = {
			text = tradition_parliament_of_bugs_trigger_desc
			OR = {
				this = culture:chi_addun
				any_parent_culture_or_above = {
					this = culture:chi_addun
				}
			}
		}
	}
	
	parameters = {
		paranoid_bonuses = yes
		paranoid_trait_more_common = yes
		hostile_action_vs_rival_bonuses = yes
		#rival_murder_decriminalized = yes  #smoking on that chi adduni pack
		content_trait_looked_down_upon = yes
		#prestige bonus to ambitious, prestige malus to lazy, big prestige malus to content
		ambition_valued = yes
	}

	character_modifier = {
        diplomacy = -1
	}

	culture_modifier = {
		cultural_acceptance_gain_mult = -0.25
	}
	
	cost = {
		prestige = {
			add = {
				value = tradition_base_cost
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
			if = {
				limit = {
					NOR = {
						any_in_list = { list = traits this = flag:ethos_egalitarian }
					}
				}
				add = {
					value = tradition_incompatible_ethos_penalty
					desc = not_egalitarian_desc
				}
			}
			
			multiply = tradition_replacement_cost_if_relevant
		}
	}
}

tradition_cynical_believers = { #Chi-Adduni/Telvanni
	category = regional

	layers = {
		0 = learning
		1 = mediterranean
		4 = speech.dds
	}

	can_pick = {
		custom_tooltip = {
			text = tradition_parliament_of_bugs_trigger_desc
			OR = {
				this = culture:chi_addun
				any_parent_culture_or_above = {
					this = culture:chi_addun
				}
			}
		}
	}
	
	parameters = {
		cynical_trait_more_common = yes
		arrogant_trait_more_common = yes
		#bonuses to cynical and arrogant, opinion malus to theologian
		cynical_believers_bonus = yes
	}

	character_modifier = {
        religious_vassal_opinion = -20
		religious_head_opinion = -20
		learning_per_prestige_level = 1
	}
	
	cost = {
		prestige = {
			add = {
				value = tradition_base_cost
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
			if = {
				limit = {
					NOR = {
						any_in_list = { list = traits this = flag:ethos_egalitarian }
					}
				}
				add = {
					value = tradition_incompatible_ethos_penalty
					desc = not_egalitarian_desc
				}
			}
			
			multiply = tradition_replacement_cost_if_relevant
		}
	}
}

tradition_pursuit_of_wealth = { #Armun-Ani/Hlaalu
	category = regional

	layers = {
		0 = steward
		1 = mediterranean
		4 = state_ransoming.dds
	}

	can_pick = {
		custom_tooltip = {
			text = tradition_pursuit_of_wealth_trigger_desc
			OR = {
				this = culture:armun_an
				any_parent_culture_or_above = {
					this = culture:armun_an
				}
			}
		}
	}
	
	parameters = {
		bossy_trait_more_common = yes
		pensive_trait_less_common = yes
		better_tax_assessor_buildings = yes
		next_level_economic_buildings = yes
		can_travel_along_rivers = yes
	}

	character_modifier = {
        diplomacy_per_piety_level = 1
		stewardship_per_prestige_level = 1
	}
	
	cost = {
		prestige = {
			add = {
				value = tradition_base_cost
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
			if = {
				limit = {
					NOR = {
						any_in_list = { list = traits this = flag:ethos_courtly }
						any_in_list = { list = traits this = flag:ethos_communal }
					#	any_in_list = { list = traits this = flag:ethos_mercantile }
					}
				}
				add = {
					value = tradition_incompatible_ethos_penalty
					#desc = not_courtly_communal_or_mercantile_desc #TODO: decide what to do with this
					desc = not_communal_or_courtly_desc
				}
			}
			
			multiply = tradition_replacement_cost_if_relevant
		}
	}
}

tradition_deshaan_agronomy = { #Marduhn-Oad/Dres
	category = regional

	layers = {
		0 = steward
		1 = mediterranean
		4 = farmland.dds
	}

	can_pick = {
		custom_tooltip = {
			text = tradition_deshaan_agronomy_trigger_desc
			OR = {
				this = culture:marduhn_oad
				any_parent_culture_or_above = {
					this = culture:marduhn_oad
				}
			}
		}
	}
	
	parameters = {
		poorly_educated_stewards_distrusted = yes
		curious_trait_more_common = yes
		rowdy_trait_less_common = yes
		can_travel_along_rivers = yes
		deshaan_agronomy_trait_values = yes
	}

	character_modifier = {
		stewardship_per_piety_level = 1
	}
	
	cost = {
		prestige = {
			add = {
				value = tradition_base_cost
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
			if = {
				limit = {
					NOR = {
						any_in_list = { list = traits this = flag:ethos_bellicose }
					}
				}
				add = {
					value = tradition_incompatible_ethos_penalty
					desc = not_bellicose_desc
				}
			}
			
			multiply = tradition_replacement_cost_if_relevant
		}
	}
}

tradition_red_mountain_might = { #Ashlander core
	category = regional

	layers = {
		0 = steward
		1 = mediterranean
		4 = mountain.dds
	}

	can_pick = {
		custom_tooltip = {
			text = tradition_red_mountain_might_trigger_desc
			OR = {
				this = culture:ashlander
				any_parent_culture_or_above = {
					this = culture:ashlander
				}
			}
		}
	}
	
	parameters = {
		stubborn_trait_more_common = yes
		martial_education_better_outcomes = yes
		diplomacy_education_worse_outcomes = yes
		poorly_educated_leaders_distrusted = yes
		scandinavian_elective_enabled = yes #rebranded as Popular Elective
		unlock_maa_ashlander_champions = yes
	}

	character_modifier = {
        controlled_province_advantage = 5
		cultural_head_fascination_mult = -0.1
		ashlands_advantage = 3
		volcanic_advantage = 3
	}

	county_modifier = {
		ashlands_levy_size = 0.2
		volcanic_levy_size = 0.2
		ashlands_supply_limit_mult = 0.25
		volcanic_supply_limit_mult = 0.25
	}
	
	cost = {
		prestige = {
			add = {
				value = tradition_base_cost
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
			if = {
				limit = {
					NOR = {
						any_in_list = { list = traits this = flag:ethos_bellicose }
						any_in_list = { list = traits this = flag:ethos_spiritual }
						any_in_list = { list = traits this = flag:ethos_stoic }
					}
				}
				add = {
					value = tradition_incompatible_ethos_penalty
					desc = not_bellicose_spiritual_or_stoic_desc
				}
			}
			
			multiply = tradition_replacement_cost_if_relevant
		}
	}
}

### Orcish Traditions

tradition_orc_core = {
    category = regional

    layers = {
		0 = martial
		1 = indian
		4 = city.dds
	}

    is_shown = {
		always = yes
	}
	can_pick = {
		custom_tooltip = {
			text = tradition_orc_core_trigger_desc
			#NOT = { has_cultural_tradition = tradition_iron_orc_core }
			OR = {
				this = culture:mountain_orc
				any_parent_culture_or_above = {
					this = culture:mountain_orc
				}
			}
		}
	}
    parameters = {
        unlock_maa_orc_marauders = yes
		wounds_and_scars_give_bonuses = yes
		can_appoint_court_wives = yes
		number_of_spouses = 3
        overseer_xp_rebate = yes
        revenge_valued = yes
		#dislikes_marrying_outside_of_culture = yes
		dislikes_marrying_outside_of_heritage = yes
		ai_doesnt_marry_outside_culture = yes
    }

	province_modifier = {
		levy_reinforcement_rate = -0.3
	}

	culture_modifier = {
		cultural_acceptance_gain_mult = -0.5
		mercenary_count_mult = 1
	}

	# doctrine_character_modifier = { # These are trying to add parameters based on a doctrine instead of modifiers - change it into a scripted trigger instead.
	# 	parameter = tenet_contempt_for_the_weak
	# 	weak_traits_looked_down_upon = yes
	# 	strong_traits_more_valued = yes
	# }

    character_modifier = {
		negate_health_penalty_add = 0.2
		negate_prowess_penalty_add = 5
		same_culture_mercenary_hire_cost_mult = -0.15
	}
	cost = {
		prestige = {
			add = {
				value = tradition_base_cost
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
			if = {
				limit = {
					NOR = {
						any_in_list = { list = traits this = flag:ethos_bellicose }
						any_in_list = { list = traits this = flag:ethos_stoic }
						any_in_list = { list = traits this = flag:ethos_communal }
					}
				}
				add = {
					value = tradition_incompatible_ethos_penalty
					desc = not_bellicose_stoic_or_communal_desc
				}
			}
			multiply = tradition_replacement_cost_if_relevant
		}
	}
	
	ai_will_do = {
		value = 100
		if = {
			limit = {
				NOR = {
					any_in_list = { list = traits this = flag:ethos_bellicose }
					any_in_list = { list = traits this = flag:ethos_stoic }
					any_in_list = { list = traits this = flag:ethos_communal }
					scope:character = {
						sub_realm_size <= medium_realm_size
					}
				}
			}
			multiply = 0.25
		}
	}
}

tradition_forlorn_peace = {
    category = regional

    layers = {
		0 = intrigue
		1 = indian
		4 = tools.dds
	}

    is_shown = {
		always = yes
	}
	can_pick = {
		custom_tooltip = {
			text = tradition_iron_orc_core_trigger_desc
			#NOT = { has_cultural_tradition = tradition_iron_orc_core }
			OR = {
				this = culture:iron_orc
				any_parent_culture_or_above = {
					this = culture:iron_orc
				}
			}
		}
	}
	
    parameters = {
        charming_trait_less_common = yes
        torturer_xp_rebate = yes
        cruelty_valued = yes
		courtiers_less_likely_to_leave_same_culture_court = yes
		dislikes_marrying_outside_of_culture = yes
		dislikes_marrying_outside_of_heritage = yes
		ai_doesnt_marry_outside_culture = yes
    }

	cost = {
		prestige = {
			add = {
				value = tradition_base_cost
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
			if = {
				limit = {
					NOR = {
						any_in_list = { list = traits this = flag:ethos_bellicose }
						any_in_list = { list = traits this = flag:ethos_stoic }
						any_in_list = { list = traits this = flag:ethos_communal }
					}
				}
				add = {
					value = tradition_incompatible_ethos_penalty
					desc = not_bellicose_stoic_or_communal_desc
				}
			}
			multiply = tradition_replacement_cost_if_relevant
		}
	}
	
	ai_will_do = {
		value = 100
		if = {
			limit = {
				NOR = {
					any_in_list = { list = traits this = flag:ethos_bellicose }
					any_in_list = { list = traits this = flag:ethos_stoic }
					any_in_list = { list = traits this = flag:ethos_communal }
					scope:character = {
						sub_realm_size <= medium_realm_size
					}
				}
			}
			multiply = 0.25
		}
	}
}

tradition_arsenal = { #for people who like strong defenses
    category = regional

    layers = {
		0 = steward
		1 = indian
		4 = tools.dds
	}

    is_shown = {
		always = yes
	}
	can_pick = {
		custom_tooltip = {
			text = tradition_arsenal_trigger_desc
			NOT = { has_cultural_tradition = tradition_castle_keepers }
			NOT = { has_cultural_tradition = tradition_metal_craftsmanship }
			OR = {
				this = culture:mountain_orc
				any_parent_culture_or_above = {
					this = culture:mountain_orc
				}
			}
		}
	}
    parameters = {
        castle_fortifications_increase = yes
		castle_grant_prestige = yes
		ai_more_likely_to_castle = yes
		ai_more_likely_to_fortify = yes
		better_armory_buildings = yes
		better_blacksmith_buildings = yes
    }
	character_modifier = {
		castle_holding_build_gold_cost = 0.2
		castle_holding_build_speed = 0.2
	}


	cost = {
		prestige = {
			add = {
				value = tradition_base_cost
				desc = BASE
				format = "BASE_VALUE_FORMAT"
			}
			if = {
				limit = {
					NOR = {
						any_in_list = { list = traits this = flag:ethos_bellicose }
						any_in_list = { list = traits this = flag:ethos_stoic }
						any_in_list = { list = traits this = flag:ethos_communal }
					}
				}
				add = {
					value = tradition_incompatible_ethos_penalty
					desc = not_bellicose_stoic_or_communal_desc
				}
			}
			multiply = tradition_replacement_cost_if_relevant
		}
	}
	
	ai_will_do = {
		value = 100
		if = {
			limit = {
				NOR = {
					any_in_list = { list = traits this = flag:ethos_bellicose }
					any_in_list = { list = traits this = flag:ethos_stoic }
					any_in_list = { list = traits this = flag:ethos_communal }
					scope:character = {
						sub_realm_size <= medium_realm_size
					}
				}
			}
			multiply = 0.25
		}
	}
}