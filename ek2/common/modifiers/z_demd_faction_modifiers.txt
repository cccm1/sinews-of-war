﻿#############################################
# DEMD Population System
# by Vertimnus
# Do not edit without making corresponding edits to the metascript source code. Bad things will happen!
#############################################

county_corruption_goblins_rampant_modifier = {
	icon = outdoors_negative
	county_opinion_add = -10
	tax_mult = -0.2
	development_growth_factor = large_development_growth_loss
}
county_corruption_undead_rampant_modifier = { # Necromancers
	icon = county_modifier_development_negative
	county_opinion_add = -10
	tax_mult = -0.2
	development_growth_factor = large_development_growth_loss
}

# DEMD_FACTION EDIT
county_corruption_falmer_rampant_modifier = {
	icon = county_modifier_development_negative
	county_opinion_add = -10
	tax_mult = -0.2
	development_growth_factor = large_development_growth_loss
}
county_corruption_forsworn_rampant_modifier = { 
	icon = county_modifier_development_negative
	county_opinion_add = -10
	tax_mult = -0.2
	development_growth_factor = large_development_growth_loss
}
county_corruption_vampires_rampant_modifier = { 
	icon = county_modifier_development_negative
	county_opinion_add = -10
	tax_mult = -0.2
	development_growth_factor = large_development_growth_loss
}
county_corruption_werewolves_rampant_modifier = {
	icon = county_modifier_development_negative
	county_opinion_add = -10
	tax_mult = -0.2
	development_growth_factor = large_development_growth_loss
}
county_corruption_cultists_rampant_modifier = { 
	icon = county_modifier_development_negative
	county_opinion_add = -10
	tax_mult = -0.2
	development_growth_factor = large_development_growth_loss
}
county_corruption_automatons_rampant_modifier = { 
	icon = county_modifier_development_negative
	county_opinion_add = -10
	tax_mult = -0.2
	development_growth_factor = large_development_growth_loss
}