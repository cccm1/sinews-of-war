﻿demd_send_trade_expedition_interaction = {

	category = interaction_category_diplomacy
	common_interaction = yes
	highlighted_reason = TRADE_EXPEDITION_FAVORABLE
	notification_text = TRADE_EXPEDITION_PROPOSAL

	desc = demd_send_trade_expedition_interaction_desc

	#special_interaction = revoke_title_interaction
	#interface = revoke_title
	#target_type = title
	#target_filter = recipient_domain_titles
	ai_maybe = no
	can_send_despite_rejection = no
	popup_on_receive = yes
	pause_on_receive = yes
	icon = icon_gold

	interface_priority = 60
	ai_min_reply_days = 4
	ai_max_reply_days = 9

	on_decline_summary = demd_send_trade_expedition_interaction_decline_summary

	# actor character giving the titles
	# recipient character receiving the titles

	is_shown = {
		always = no # under construction
		scope:recipient = {
			is_landed = yes
		}
	}
	
	is_valid_showing_failures_only = {
		scope:actor = { NOT =  { is_at_war_with = scope:recipient } }
		scope:actor = { expedition_sender_has_port = yes }
		scope:actor = { exists = cp:councillor_steward }
		scope:recipient = { expedition_sender_has_port = yes }
	}

	can_send = {
		
	}
	
	# Highlighted in the interaction menu when right-clicking a character
	is_highlighted = {
		always = no
	}

	send_option = {
		is_shown = { always = yes }
		is_valid = { scope:actor = { gold > expedition_monumental_cost } }
		flag = expedition_monumental
		localization = demd_expedition_monumental
		starts_enabled = { 
			scope:actor = { gold > expedition_monumental_cost  } 
		}
	}
	send_option = {
		is_shown = { always = yes }
		is_valid = { scope:actor = { gold > expedition_massive_cost } }
		flag = expedition_massive
		localization = demd_expedition_massive
		starts_enabled = { 
			scope:actor = { 
				gold > expedition_massive_cost 
				gold < expedition_monumental_cost 
			} 
		}
	}
	send_option = {
		is_shown = { always = yes }
		is_valid = { scope:actor = { gold > expedition_major_cost } }
		flag = expedition_major
		localization = demd_expedition_major
		starts_enabled = { 
			scope:actor = { 
				gold > expedition_medium_cost 
				gold < expedition_massive_cost 
			} 
		}
	}
	send_option = {
		is_shown = { always = yes }
		is_valid = { scope:actor = { gold > expedition_medium_cost } }
		flag = expedition_medium
		localization = demd_expedition_medium
		starts_enabled = { 
			scope:actor = { 
				gold > expedition_minor_cost 
				gold < expedition_major_cost 
			} 
		}
	}
	send_option = {
		is_shown = { always = yes }
		is_valid = { scope:actor = { gold > expedition_minor_cost } }
		flag = expedition_minor
		localization = demd_expedition_minor
		starts_enabled = { 
			scope:actor = { gold < expedition_minor_cost } 
		}
	}
		
	send_options_exclusive = yes

	on_send = {
		scope:actor = {
			add_character_flag = {
				flag = flag_hostile_actions_disabled_delay
				days = 10
			}
		}
	}

	on_auto_accept = {
		scope:recipient = {
			trigger_event = char_interaction.0200
		}
	}

	on_accept = {
		scope:actor = {
			if = {
				limit = { scope:expedition_minor = yes }
				remove_short_term_gold = expedition_minor_cost
			}
			else_if = {
				limit = { scope:expedition_medium = yes }
				remove_short_term_gold = expedition_medium_cost
			}
			else_if = {
				limit = { scope:expedition_major= yes }
				remove_short_term_gold = expedition_major_cost
			}
			else_if = {
				limit = { scope:expedition_massive = yes }
				remove_short_term_gold = expedition_massive_cost
			}
			else_if = {
				limit = { scope:expedition_monumental = yes }
				remove_short_term_gold = expedition_monumental_cost
			}
		}
	}

	on_decline = {
		
	}

	auto_accept = {

	}
	ai_accept = {
		base = 0 # Try to make it 0 for most interactions
		
		modifier = {
			add = 20
			scope:recipient = {
				has_trait = greedy
			}
			desc = INTERACTION_GREEDY
		}

		ai_value_modifier = {
			who = scope:recipient
			ai_greed = {
				if = {
					limit = {
						scope:recipient = { NOT = { ai_greed = 0 } }
					}
					value = 0.75
				}
			}
		}
		modifier = {
			add = intimidated_halved_reason_value
			scope:recipient = {
				has_dread_level_towards = {
					target = scope:actor
					level = 1
				}
			}
			desc = INTIMIDATED_REASON
		}
		modifier = {
			add = cowed_halved_reason_value
			scope:recipient = {
				has_dread_level_towards = {
					target = scope:actor
					level = 2
				}
			}
			desc = COWED_REASON
		}
		modifier = { #Comparative military strength.
			desc = offer_vassalization_interaction_aibehavior_power_tt
	  	  	add = {
				value = 1
				subtract = {
					value = scope:recipient.current_military_strength
					divide = { value = scope:actor.current_military_strength min = 1 }
				}
				multiply = 50
				max = 20
	  		}
		}

	}

	# AI	
	ai_targets = {
		ai_recipients = neighboring_rulers
		ai_recipients = peer_vassals
		max = 10
	}
	ai_targets = {
		ai_recipients = liege
	}
	ai_targets = {
		ai_recipients = vassals
		max = 10
	}
	ai_frequency = 36

	ai_potential = {
		primary_title.tier > tier_county
	}

	ai_will_do = {
		base = 0

		# Don't send expeditions to characters you hate
		modifier = {
			scope:actor = {
				OR = {
					has_relation_rival = scope:recipient
					has_relation_nemesis = scope:recipient
				}
			}
			add = -100
		}

		# send expeditions to characters you like
		modifier = {
			scope:actor = {
				OR = {
					has_relation_friend = scope:recipient
					has_relation_best_friend = scope:recipient
					has_relation_lover = scope:recipient
					has_relation_soulmate = scope:recipient
					is_parent_of = scope:recipient
					is_grandparent_of = scope:recipient
					is_great_grandparent_of = scope:recipient
				}
			}
			add = 100
		}
		
		modifier = { # The AI will always use a Hook if they can
			scope:hook = yes
			add = 1
		}

		modifier = { # Avoid doing mid-war
			factor = 0
			scope:actor = { is_at_war = yes }
		}
	}
}
