﻿cultureEconomy = {	
	set_variable = { name = culture_total_development value = culture_total_development }
	set_variable = { name = dominant_faith value = faith:imperial_cult } # for safety in case calc_culture_dominant_faith fucks up as it sometimes does
	set_variable = { name = dominant_faith value = calc_culture_dominant_faith }
	
	change_variable = { name = vigor add = vigor_delta }
	clamp_variable = { name = vigor min = 0 max = 100 }	
}