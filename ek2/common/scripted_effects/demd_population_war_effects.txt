﻿#############################################
# DEMD Population System
# by Vertimnus
# Do not edit without making corresponding edits to the metascript source code. Bad things will happen!
#############################################

#####################################################################################################

demd_occupy_effect = {
	save_scope_as = conqueror
	add_gold = scope:county.demd_occupy_gold_value
	scope:county = {
		save_scope_value_as = {
			name = sack_status
			value = flag:occupied
		}
		
		demd_occupy_effect_county = yes
	}
}

demd_occupy_effect_county = {
	if = {
		limit = { holder = { NOT = { has_perk = enduring_hardships_perk } } }
		change_county_control = occupy_county_control
		change_development_progress_with_overflow = occupy_development_loss
		threat_damage_scale_from_public_order_setting_siege_tooltip = yes
	}	
	add_county_modifier = {	#Prevent the county from being sacked again for quite a while.
		modifier = recently_sacked_modifier
		years = 20
	}
	random = { 
		chance = 5
		custom_tooltip = demd_siege_fire_breaks_out
		trigger_event = { id = demd_pop_fire.1001 days = { 1 7 } }
	}
	random = { 
		chance = 0
		modifier = {
			NOT = { has_county_modifier = plague_immunity }
			add = 2
		}
		custom_tooltip = demd_siege_plague_breaks_out
		trigger_event = { id = demd_pop_disease.1001 days = { 8 30 } }
	}
}

demd_sack_effect = {
	add_dread = minor_dread_gain
	add_gold = scope:county.demd_sack_gold_value	
	stress_impact = {
		compassionate = medium_stress_impact_gain
		forgiving = medium_stress_impact_gain
		sadistic = medium_stress_impact_loss
		vengeful = medium_stress_impact_loss
		wrathful = medium_stress_impact_loss
	}
	if = {
		limit = { faith = scope:county.faith }
		add_piety = medium_piety_loss
		if = {
			limit = { exists = religious_head }
			religious_head = {
				add_opinion = {
					target = scope:occupier
					modifier = sacked_coreligionist_opinion
				}
			}
		}
	}
	if = {
		limit = { culture = scope:county.culture }
		add_character_modifier = {
			modifier = sacked_same_culture_county
			years = 20
		}
	}
	save_scope_as = conqueror
	scope:county = {
		save_scope_value_as = {
			name = sack_status
			value = flag:sacked
		}
		
		demd_sack_effect_county = yes
		holder = {
			add_opinion = {
				target = scope:occupier
				modifier = sacked_me_opinion
			}
			every_liege_or_above = {
				limit = { NOT = { this = scope:occupier } }
				add_opinion = {
					target = scope:occupier
					modifier = sacked_vassal_opinion
				}
			}
		}
	}	
}

demd_sack_effect_county = {
	if = {
		limit = { NOT = { culture = scope:occupier.culture } }
		culture = { change_cultural_acceptance = { target = scope:occupier.culture value = demd_sack_acceptance_change desc = demd_sack_acceptance_change } }
	}
	else = {
		culture = { demd_alter_vigor = { operation = subtract amount = small } }
	}
	
	if = {
		limit = { holder = { NOT = { has_perk = enduring_hardships_perk } } }
		change_county_control = sack_county_control
		change_development_progress_with_overflow = sack_development_loss
		threat_damage_scale_from_public_order_setting_siege_tooltip = yes
	}	
	random = { 
		chance = 25
		custom_tooltip = demd_siege_fire_breaks_out
		trigger_event = { id = demd_pop_fire.1001 days = { 1 7 } }
	}
	random = { 
		chance = 0
		modifier = {
			NOT = { has_county_modifier = plague_immunity }
			add = 10
		}
		custom_tooltip = demd_siege_plague_breaks_out
		trigger_event = { id = demd_pop_disease.1001 days = { 8 30 } }
	}
	if = {
		limit = { scope:occupier = { can_take_slaves = yes } }		
		scope:capital = { change_development_progress_with_overflow = scope:county.take_slaves_development_gain }
		#custom_tooltip = demd_slaves_taken
		scope:county = { change_development_progress_with_overflow = take_slaves_development_loss }		
	}
	add_county_modifier = {	#Prevent the county from being sacked again for quite a while.
		modifier = recently_sacked_modifier
		years = 20
	}
}

demd_raze_effect = {
	add_dread = medium_dread_gain
	add_gold = scope:county.demd_raze_gold_value
	stress_impact = {
		compassionate = major_stress_impact_gain
		sadistic = minor_stress_impact_loss
		vengeful = minor_stress_impact_loss
		wrathful = minor_stress_impact_loss
	}
	if = {
		limit = { faith = scope:county.faith }
		add_piety = major_piety_loss
		if = {
			limit = { exists = religious_head }
			religious_head = {
				add_opinion = {
					target = scope:occupier
					modifier = razed_coreligionist_opinion
				}
			}
		}
	}
	if = {
		limit = { culture = scope:county.culture }
		add_character_modifier = {
			modifier = razed_same_culture_county
			years = 20
		}
	}
	save_scope_as = conqueror
	scope:county = {
		save_scope_value_as = {
			name = sack_status
			value = flag:razed
		}
		
		demd_raze_effect_county = yes		
		holder = {
			add_opinion = {
				target = scope:occupier
				modifier = razed_me_opinion
			}
			every_liege_or_above = {
				limit = { NOT = { this = scope:occupier } }
				add_opinion = {
					target = scope:occupier
					modifier = razed_vassal_opinion
				}
			}
		}
	}
}

demd_raze_effect_county = {
	if = {
		limit = { NOT = { culture = scope:occupier.culture } }
		culture = { change_cultural_acceptance = { target = scope:occupier.culture value = demd_raze_acceptance_change desc = demd_raze_acceptance_change } }
	}
	else = {
		culture = { demd_alter_vigor = { operation = subtract amount = small } }
	}
	
	if = {
		limit = { holder = { NOT = { has_perk = enduring_hardships_perk } } }
		change_county_control = raze_county_control
		change_development_progress_with_overflow = raze_development_loss
		threat_damage_scale_from_public_order_setting_siege_tooltip = yes
	}
	random = { 
		chance = 50
		custom_tooltip = demd_siege_fire_breaks_out
		trigger_event = { id = demd_pop_fire.1001 days = { 1 7 } }
	}
	random = { 
		chance = 0
		modifier = {
			NOT = { has_county_modifier = plague_immunity }
			add = 20
		}
		custom_tooltip = demd_siege_plague_breaks_out
		trigger_event = { id = demd_pop_disease.1001 days = { 8 30 } }
	}
	if = {
		limit = { scope:occupier = { can_take_slaves = yes } }		
		scope:capital = { change_development_progress_with_overflow = scope:county.take_slaves_development_gain }
		#custom_tooltip = demd_slaves_taken
		scope:county = { change_development_progress_with_overflow = take_slaves_development_loss }		
	}
	add_county_modifier = {	#Prevent the county from being sacked again for quite a while.
		modifier = recently_sacked_modifier
		years = 20
	}	
}
