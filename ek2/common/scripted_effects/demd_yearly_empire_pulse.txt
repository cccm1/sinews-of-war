﻿empire_title_pulse = {
	if = {
		limit = { is_title_created = yes }

		if = { 
			limit = { NOT = { has_variable = imperial_decay } } 
			set_variable = { name = imperial_decay value = 0 } 
		}
		else = { 
			change_variable = { name = imperial_decay add = imperial_decay_delta } 
			clamp_variable = { name = imperial_decay min = 0 max = imperial_decay_max }
		}
	}	
	else = { set_variable = { name = imperial_decay value = 0 } }
}