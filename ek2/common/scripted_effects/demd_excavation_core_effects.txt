﻿demd_initiate_threat_event_damage = {
	demd_excavation_add_threat_modifier = yes
	change_county_control = control_loss_from_threat_level_event
	change_development_progress_with_overflow = development_progress_loss_from_threat_level_event
	threat_damage_scale_from_public_order_setting_tooltip = yes
}

demd_initiate_critical_threat_event_damage = {
	demd_excavation_add_threat_modifier = yes
	change_county_control = critical_control_loss_from_threat_level_event
	change_development_progress_with_overflow = critical_development_progress_loss_from_threat_level_event
	threat_damage_scale_from_public_order_setting_tooltip = yes
}

demd_excavation_add_threat_modifier = {
	scope:story = {
		if = {
			limit = { var:occupants_type = flag:demd_goblins }
			scope:excavation_county = { add_county_modifier = { modifier = county_corruption_goblins_rampant_modifier years = 5 } }
		}
		else_if = {
			limit = { var:occupants_type = flag:demd_falmer }
			scope:excavation_county = { add_county_modifier = { modifier = county_corruption_falmer_rampant_modifier years = 5 } }
		}
		else_if = {
			limit = { var:occupants_type = flag:demd_forsworn }
			scope:excavation_county = { add_county_modifier = { modifier = county_corruption_forsworn_rampant_modifier years = 5 } }
		}
		else_if = {
			limit = { var:occupants_type = flag:demd_necromancers }
			scope:excavation_county = { add_county_modifier = { modifier = county_corruption_undead_rampant_modifier years = 5 } }
		}
		else_if = {
			limit = { var:occupants_type = flag:demd_vampires }
			scope:excavation_county = { add_county_modifier = { modifier = county_corruption_vampires_rampant_modifier years = 5 } }
		}
		else_if = {
			limit = { var:occupants_type = flag:demd_werewolves }
			scope:excavation_county = { add_county_modifier = { modifier = county_corruption_werewolves_rampant_modifier years = 5 } }
		}
		else_if = {
			limit = { var:occupants_type = flag:demd_cultists }
			scope:excavation_county = { add_county_modifier = { modifier = county_corruption_cultists_rampant_modifier years = 5 } }
		}
		else = { scope:excavation_county = { add_county_modifier = { modifier = county_corruption_bandits_rampant_modifier years = 5 } } }
	}
}

demd_send_notification_of_new_ruins_owner = {
	save_scope_as = excavation_story
	var:excavation_county = { save_scope_as = excavation_county }
	var:ruins_faith = { save_scope_as = ruins_faith }
	var:ruins_culture = { save_scope_as = ruins_culture }
	var:occupants_faith = { save_scope_as = occupants_faith }
	var:occupants_culture = { save_scope_as = occupants_culture }
	var:excavation_county.holder = {
		send_interface_message = {
			type = event_excavation_bad
			title = demd_excavation_new_occupier_t
			desc = demd_excavation_new_occupier_desc
		}
	}
}

demd_after_ruins_occupant_creation_effect = {
	if = { 
		limit = { faith = faith:molag_cult } 
		add_trait = vampire_character 
		add_trait = necromancer
		random_list = {
			10 = { add_trait = education_magical_ability_3 }
			10 = { add_trait = education_magical_ability_4 }
		}
	}
	else_if = {
		limit = { religion = religion:necromantic_religion }
		add_trait = necromancer
		random_list = {
			10 = { add_trait = education_magical_ability_3 }
			10 = { add_trait = education_magical_ability_4 }
		}
		random_list = {
			20 = { add_trait = lich_character }
			80 = {  }
		}
	}
	else_if = {
		limit = { faith = faith:hircine_cult } 
		add_trait = lycan_character 
	}	
}

##########################################################################

initialize_excavation_effect = {
	save_scope_as = excavation_county

	# create story cycle and subtract gold
	scope:excavation_county.holder = {
		create_story = {
			type = story_cycle_excavation
			save_scope_as = excavation_story
		}
	}
	
	scope:excavation_story = { 
		
		set_variable = { name = excavation_county value = scope:excavation_county }
		var:excavation_county = { set_variable = { name = county_excavation value = scope:excavation_story } }
		set_variable = { name = excavation_owner value = var:excavation_county.holder }
		
		set_variable = { name = ruins_type value = flag:demd_$TYPE$ }
		set_variable = { name = ruins_type_unique value = flag:demd_$UNIQUE_TYPE$ }
		
		link_ruins_type_to_faith_culture = yes	
		select_random_ruins_occupants = yes
		
		reroll_excavation_modifiers = yes
		
		roll_for_ruins_quality_complexity = yes		
		set_variable = { name = ruins_quality value = ruins_quality_max }
		set_variable = { name = ruins_complexity value = ruins_complexity_max }
		
		# TEST CODE
		# random = {
			# chance = 100
			# var:excavation_county.holder = {
				# initialize_excavation_quest = { QUEST_ID = strange_magic QUEST_REWARD_GOLD = excavation_quest_medium_gold_reward QUEST_REWARD_PRESTIGE = excavation_quest_medium_prestige_reward QUEST_REWARD_PIETY = excavation_quest_medium_piety_reward EXCAVATION = scope:excavation_story QUEST_GIVER = character:volkihar_1002 QUEST_OWNER = scope:excavation_county.holder }
			# }
		# }
	}
}

start_excavation_effect = {
	start_excavation_core_effect = yes
	# subtract gold
	scope:patron = { 
		if = {
			limit = { is_ai = no }
			remove_short_term_gold = scope:excavation_story.excavation_start_cost 
		}
	}
	if = {
		limit = {
			scope:patron.culture = { 
				OR = {
					has_cultural_parameter = excavation_start_piety_cost
					AND = {
						scope:patron.faith = { has_doctrine_parameter = excavation_start_same_heritage_piety_cost }
						has_same_culture_heritage = prev.var:ruins_culture
					}
				}
			}
		}
		scope:patron = { add_piety = excavation_start_piety_cost }
	}
}

start_excavation_core_effect = {
	var:excavation_county = { save_scope_as = excavation_county }
	save_scope_as = excavation_story
	custom_tooltip = excavation_starts_in_county
	custom_tooltip = excavation_cleared_in_county
	custom_tooltip = excavation_quest_completion_opportunity
	# Link story cycle to new owner
	hidden_effect = {
		excavation_set_owner = { TARGET = scope:patron }
		excavation_set_leader = yes
		set_variable = excavation_is_active
		set_variable = { name = excavation_quality value = excavation_quality_base }
	}
}

clear_ruins_effect = {
	clear_ruins_core_effect = yes
	# subtract gold
	scope:patron = { 
		if = {
			limit = { is_ai = no }
			remove_short_term_gold = scope:excavation_story.excavation_clear_cost 
		}
	}
	if = {
		limit = {
			scope:patron.culture = { 
				OR = {
					has_cultural_parameter = excavation_start_piety_cost
					AND = {
						scope:patron.faith = { has_doctrine_parameter = excavation_start_same_heritage_piety_cost }
						has_same_culture_heritage = prev.var:ruins_culture
					}
				}
			}
		}
		scope:patron = { add_piety = excavation_clear_piety_cost }
	}
}

clear_ruins_core_effect = {
	var:excavation_county = { save_scope_as = excavation_county }
	save_scope_as = excavation_story
	save_scope_as = story
	custom_tooltip = excavation_cleared_in_county
	custom_tooltip = excavation_quest_completion_opportunity
	set_variable = not_excavating_ruins # to disable options in the start event
	scope:patron = { trigger_event = { on_action = excavation_beginning } }
	set_variable = { name = last_cleared_year value = current_year }
}

excavation_set_leader = {
	save_scope_as = story
	story_owner = {
		every_court_position_holder = {
			type = antiquarian_court_position
			scope:story = { set_variable = { name = excavation_leader value = prev } }
			save_scope_as = excavation_leader
		}
	}
}

##########################################################################

excavation_story_standard_setup = {
	set_variable = { name = excavation_stage value = 1 }
	set_variable = { name = threat_level value = { value = { threat_level_game_start_min threat_level_game_start_max } round = yes } }
	set_variable = { name = minor_artifacts value = 0 }
	set_variable = { name = ruins_quality value = 1 }
	set_variable = { name = ruins_complexity value = 1 }
	set_variable = { name = excavation_quality value = excavation_quality_base }
	set_variable = { name = excavation_stage_progress value = 0 }
	set_variable = { 
		name = last_excavated_year 
		value = { 
			value = current_year 
			subtract = {
				value = excavation_cooldown_years 
				add = 1
			}
		}
	}
	set_variable = { 
		name = last_cleared_year 
		value = { 
			value = current_year 
			subtract = {
				value = clear_ruins_cooldown_years
				add = 1
			}
		}
	}
}

excavation_story_standard_cleanup = {
	if = {
		limit = { var:ruins_type_unique = flag:demd_none }
		set_variable = { name = excavation_stage value = 1 }
		set_variable = { name = minor_artifacts value = 0 }
		set_variable = { name = excavation_quality value = excavation_quality_base }
		set_variable = { name = excavation_stage_progress value = 0 }
		clear_variable_list = unavailable_events
		remove_variable = excavation_leader
		excavation_set_owner = { TARGET = var:excavation_county.holder }
		remove_variable = excavation_is_active
		remove_variable = has_had_beginning_event
		set_variable = { name = last_excavated_year value = current_year }
		set_variable = { name = last_cleared_year value = current_year }
	}
	else = { end_story = yes }
}

excavation_story_standard_inheritance = {
	if = {
		limit = { exists = story_owner.player_heir }
		excavation_set_owner = { TARGET = story_owner.player_heir }
	}
	#else = {
	#	excavation_set_owner = { TARGET = dummy_male }
	#}
}

excavation_set_owner = {
	if = {
		limit = { $TARGET$ = { is_alive = yes } }
		
		# Transfer excavation itself
		make_story_owner = $TARGET$
		set_variable = { name = excavation_owner value = $TARGET$ }
		
		# Transfer active quests along with excavation
		every_in_list = {
			variable = active_quests
			excavation_quest_set_owner = { TARGET = $TARGET$ }
		}
	}
}

##########################################################################

alter_excavation_minor_artifacts = {
	custom_tooltip = excavation_$operation$_minor_artifacts_$amount$
	scope:excavation_story = {
		change_variable = { name = minor_artifacts $operation$ = excavation_minor_artifacts_$amount$ }
		clamp_variable = { name = minor_artifacts min = excavation_minor_artifacts_min max = excavation_minor_artifacts_max }
	}
}

alter_excavation_quality = {
	custom_tooltip = excavation_$operation$_quality_$amount$
	scope:excavation_story = {
		change_variable = { name = excavation_quality $operation$ = excavation_quality_$amount$ }
		clamp_variable = { name = excavation_quality min = excavation_quality_min max = excavation_quality_max }
	}
}

alter_ruins_quality = {
	custom_tooltip = ruins_$operation$_quality_$amount$
	scope:excavation_story = {
		change_variable = { name = ruins_quality $operation$ = ruins_quality_$amount$ }
		clamp_variable = { name = ruins_quality min = ruins_quality_min max = ruins_quality_max }
	}
}

set_county_ruins_quality_bonus = { # for game start setup
	if = {
		limit = { has_variable = county_excavation }
		var:county_excavation = { 
			set_variable = { name = ruins_quality_max_bonus value = $VALUE$ } 
			set_variable = { name = ruins_quality value = ruins_quality_max }
		}
	}
}

change_county_ruins_type = {
	if = {
		limit = { has_variable = county_excavation }
		var:county_excavation = {
			change_ruins_type = { TYPE = $TYPE$ }
			link_ruins_type_to_faith_culture = yes
			reroll_excavation_modifiers = yes
			set_variable = { name = ruins_complexity value = ruins_complexity_max }
			set_variable = { name = ruins_quality value = ruins_quality_max }
		}
	}
}
change_ruins_type = { # for game start setup
	set_variable = { name = ruins_type value = flag:demd_$TYPE$ } 
}

alter_ruins_complexity = {
	custom_tooltip = ruins_$operation$_complexity_$amount$
	scope:excavation_story = {
		change_variable = { name = ruins_complexity $operation$ = ruins_complexity_$amount$ }
		clamp_variable = { name = ruins_complexity min = ruins_complexity_min max = ruins_complexity_max }
	}
}

set_county_ruins_complexity_bonus = { # for game start setup
	if = {
		limit = { has_variable = county_excavation }
		var:county_excavation = { 
			set_variable = { name = ruins_complexity_max_bonus value = $VALUE$ } 
			set_variable = { name = ruins_complexity value = ruins_complexity_max }
		}
	}
}

alter_excavation_stage_progress = {
	custom_tooltip = excavation_$operation$_progress_$amount$
	scope:excavation_story = {
		change_variable = { name = excavation_stage_progress $operation$ = excavation_progress_$amount$ }
		clamp_variable = { name = excavation_stage_progress min = 0 max = 2000000 }
	}
}

alter_excavation_threat_level = {
	custom_tooltip = excavation_$operation$_threat_level_$amount$
	scope:excavation_story = {
		change_variable = { name = threat_level $operation$ = threat_level_$amount$ }
		clamp_variable = { name = threat_level min = 0 max = threat_level_max }
	}
}

#########################################################################

excavation_story_inactive_pulse = {
	change_variable = { name = threat_level add = threat_level_delta }
	clamp_variable = { name = threat_level min = threat_level_min max = threat_level_max }
	
	
	if = { 
		limit = { excavation_threat_level_is_critical = yes } # I'm in danger
		random = {
			chance = threat_level_event_chance
			if = {
				limit = { excavation_has_active_quests = no }
				var:excavation_county.holder = { trigger_event = { on_action = excavation_quest_initiation } } 
			}
			else = {
				var:excavation_county.duchy = {
					every_in_de_jure_hierarchy = {
						limit = { tier = tier_county }
						holder = { add_to_temporary_list = threat_event_county_holders }
					}
				}
				every_in_list = {
					list = threat_event_county_holders
					limit = { is_alive = yes }
					trigger_event = { on_action = excavation_threat } # DEBUG
				}
			}			
		}		
	}
}
excavation_story_minor_pulse = {
	var:excavation_county.holder = { trigger_event = { on_action = excavation_minor } }	
}

excavation_story_occupier_pulse = {
	if = {
		limit = { excavation_threat_level_is_critical = no }
		select_random_ruins_occupants = yes
	}
}

excavation_story_active_pulse = {
	if = {
		limit = { has_variable = excavation_is_active }
		
		if = { # quick check to make sure story owner even exists, which can be an issue surprisingly
			limit = { exists = story_owner }
			story_owner = { save_scope_as = story_owner }
		}
		else = { excavation_set_owner = { TARGET = var:excavation_county.holder } }
		
		
		if = {	# check if owner is valid
			limit = { excavation_owner_is_valid = no }
			var:excavation_county = {
				save_scope_as = excavation_county
				holder = {
					save_scope_as = new_owner
					send_interface_message = {
						type = event_excavation_good
						title = demd_excavation_transfer_control_new_owner_t
						desc = demd_excavation_transfer_control_new_owner_desc
					}
				}
			}
			scope:story_owner = {
				send_interface_message = {
					type = event_excavation_bad
					title = demd_excavation_transfer_control_old_owner_t
					desc = demd_excavation_transfer_control_old_owner_desc
				}
			}
			excavation_set_owner = { TARGET = var:excavation_county.holder }
		}
		
		if = { # check if leader is valid
			limit = { story_owner = { employs_court_position = antiquarian_court_position } }
			
			# Set leader from our antiquarian
			excavation_set_leader = yes	
			
			if = { # sadly can't do this in the story setup block due to var issues
				limit = { NOT = { has_variable = has_had_beginning_event } }
				story_owner = { trigger_event = { on_action = excavation_beginning } }
				set_variable = has_had_beginning_event
			}
			else = {
				# Advance excavation progress variable
				change_variable = { name = excavation_stage_progress add = monthly_excavation_progress }
				
				# If we have finished a stage
				if = {
					limit = { var:excavation_stage_progress > excavation_stage_progress_max }
					
					# Reset stage progress
					set_variable = { name = excavation_stage_progress value = 0 }
					
					if = { # If current stage is the end stage
						limit = { var:excavation_stage >= excavation_stage_max }
						
						# Send final stage event
						random_list = {
							0 = {
								modifier = {
									always = yes
									add = chance_of_positive_excavation_end_event
								}
								story_owner = { trigger_event = { on_action = excavation_final_positive } }
							}
							100 = {
								modifier = {
									always = yes
									add = neg_chance_of_positive_excavation_end_event
								}
								story_owner = { trigger_event = { on_action = excavation_final_negative } }
							}
						}
						
						
						# story cleanup
						if = {
							limit = { var:minor_artifacts > 0 }
							story_owner = { trigger_event = { id = demd_excavation.0001 days = 1 } } # sell minor artifacts
						}
						else = { excavation_story_standard_cleanup = yes }							
					}
					else = { # If current stage is not end stage
					
						# Send stage end event
						story_owner = { trigger_event = { on_action = excavation_intermediate } }
						change_variable = { name = excavation_stage add = 1 }
										
					}
				}
			}
		}
	}
}

##########################################################################

excavation_story_event_standard_setup = {
	save_scope_as = story_owner 
	save_scope_as = excavation_owner # for events
	scope:story = {
		save_scope_as = excavation_story
		var:excavation_county = { save_scope_as = excavation_county }
		
		var:ruins_type = { save_scope_as = ruins_type }
		var:ruins_faith = { save_scope_as = ruins_faith }
		var:ruins_culture = { save_scope_as = ruins_culture }
		
		var:occupants_type = { save_scope_as = occupants_type }
		var:occupants_faith = { save_scope_as = occupants_faith }
		var:occupants_culture = { save_scope_as = occupants_culture }
		
		scope:story_owner = {
			every_court_position_holder = {
				type = antiquarian_court_position
				save_scope_as = excavation_leader
				
				save_scope_as = inspiration_owner # for artifact effects
			}
			if = {
				limit = { exists = cp:councillor_marshal }
				cp:councillor_marshal = { save_scope_as = marshal }
			}
		}	
		save_scope_value_as = { # for artifact creation
			name = wealth
			value = scope:story.var:excavation_quality
		}
		save_scope_value_as = { # for artifact creation
			name = quality
			value = scope:story.var:excavation_quality
		}
	}
}

excavation_final_story_success_standard_effect = {
	alter_ruins_quality = { operation = subtract amount = small }
	alter_ruins_complexity = { operation = subtract amount = small }
	excavation_upgrade_leader_skill_effect = yes
}

excavation_final_story_failure_standard_effect = {
	alter_ruins_complexity = { operation = subtract amount = small }
	excavation_upgrade_leader_skill_effect = yes
}

##########################################################################

set_antiquarian_effect = {
	$CANDIDATE$ = {
		if = {
			limit = { 
				NOT = { liege = $EMPLOYER$ }
			}
			$EMPLOYER$ = { add_courtier = $CANDIDATE$ }
		}
	}
	$EMPLOYER$ = {
		#Replace existing court physician if one exists
		if = {
			limit = {
				employs_court_position = antiquarian_court_position
				any_court_position_holder = {
					type = antiquarian_court_position
					count > 0
				}
			}
			every_court_position_holder = {
				type = antiquarian_court_position
				save_temporary_scope_as = old_antiquarian

				$EMPLOYER$ = {
					replace_court_position = {
						recipient = $CANDIDATE$
						holder = scope:old_antiquarian
						court_position = antiquarian_court_position
					}
					reverse_add_opinion = {
						target = scope:old_antiquarian
						modifier = disappointed_opinion
						opinion = -15
					}
					reverse_add_opinion = {
						target = $CANDIDATE$
						modifier = hired_me_opinion
					}
				}
			}
		}
		#Else just straight up appoint
		else = {
			appoint_court_position = {
				recipient = $CANDIDATE$
				court_position = antiquarian_court_position
			}
			reverse_add_opinion = {
				target = $CANDIDATE$
				modifier = hired_me_opinion
			}
		}
	}
}

##########################################################################

excavation_upgrade_leader_skill_effect = {
	scope:excavation_leader = {
		
		if = {
			limit = { has_trait = archaeologist_1 }
			random = { 
				chance = 40
				hidden_effect = { remove_trait = archaeologist_1 }
				add_trait = archaeologist_2
			}			
		}
		else_if = {
			limit = { has_trait = archaeologist_2 }
			random = { 
				chance = 20
				hidden_effect = { remove_trait = archaeologist_2 }
				add_trait = archaeologist_3
			}
		}
		else_if = {
			limit = { has_trait = archaeologist_3 }
			# nothing
		}
		else = {
			add_trait = archaeologist_1
		}
		
		if = {
			limit = { NOT = { knows_language_of_culture = scope:story.var:ruins_culture } }
			random = { 
				chance = 30
				learn_language_of_culture = scope:story.var:ruins_culture
			}	
		}
	}
}

##########################################################################

threat_damage_scale_from_public_order_setting_siege_tooltip = {
	if = {
		limit = { threat_damage_scale_from_public_order_edict > 0 }
		custom_tooltip = threat_damage_scale_from_public_order_setting_negative_siege_tooltip
	}
	else_if = {
		limit = { threat_damage_scale_from_public_order_edict < 0 }
		custom_tooltip = threat_damage_scale_from_public_order_setting_positive_siege_tooltip
	}
}

threat_damage_scale_from_public_order_setting_tooltip = {
	if = {
		limit = { threat_damage_scale_from_public_order_edict > 0 }
		custom_tooltip = threat_damage_scale_from_public_order_setting_negative_tooltip
	}
	else_if = {
		limit = { threat_damage_scale_from_public_order_edict < 0 }
		custom_tooltip = threat_damage_scale_from_public_order_setting_positive_tooltip
	}
}

disaster_damage_scale_from_sanitation_setting_tooltip = {
	if = {
		limit = { disaster_damage_scale_from_sanitation_edict > 0 }
		custom_tooltip = disaster_damage_scale_from_sanitation_setting_negative_tooltip
	}
	else_if = {
		limit = { disaster_damage_scale_from_sanitation_edict < 0 }
		custom_tooltip = disaster_damage_scale_from_sanitation_setting_positive_tooltip
	}
}

demd_excavation_test_effect_increase_threat = {
	hidden_effect  = {
		every_held_title = {
			limit = { 
				tier = tier_county 
				has_variable = county_excavation
			}
			var:county_excavation = {
				save_scope_as = excavation_story
				alter_excavation_threat_level = { operation = add amount = huge }
			}
		}
	}
}