﻿destruction_novice_perk = {
	lifestyle = magic_lifestyle
	tree = destruction
	position = { 0 0 }
	icon = node_martial
	
	auto_selection_weight = {
		value = 11
		if = {
			limit = { has_students_magicka_background = yes }
			add = 1989
		}
		if = {
			limit = {
				can_start_new_lifestyle_tree_trigger = no
			}
			multiply = 0
		}
		if = {
			limit = {
				is_ai = yes
			}
			multiply = 0
		}
	}
	
	effect = {	
		custom_description_no_bullet = {
			text = destruction_novice_perk_effect
		}
		custom_description_no_bullet = {
			text = destruction_novice_perk_ritual_effect
		}
		if = {
            limit = { culture = { has_cultural_parameter = destruction_xp_rebate } } ###EK EDIT: new parameter
            add_magic_lifestyle_xp = 330
        }
	}
}

destruction_apprentice_perk = {
	lifestyle = magic_lifestyle
	tree = destruction
	position = { 0 1 }
	icon = node_martial
	
	parent = destruction_novice_perk
	
	effect = {	
		custom_description_no_bullet = {
			text = destruction_apprentice_perk_effect
		}
		if = {
            limit = { culture = { has_cultural_parameter = destruction_xp_rebate } } ###EK EDIT: new parameter
            add_magic_lifestyle_xp = 330
        }
	}
}

destruction_journeyman_perk = {
	lifestyle = magic_lifestyle
	tree = destruction
	position = { 0 2 }
	icon = node_martial
	
	parent = destruction_apprentice_perk
	
	effect = {	
		custom_description_no_bullet = {
			text = destruction_journeyman_perk_effect
		}
		if = {
            limit = { culture = { has_cultural_parameter = destruction_xp_rebate } } ###EK EDIT: new parameter
            add_magic_lifestyle_xp = 330
        }
	}
}

destruction_expert_perk = {
	lifestyle = magic_lifestyle
	tree = destruction
	position = { 0 3 }
	icon = node_martial
	
	parent = destruction_journeyman_perk
	
	effect = {	
		custom_description_no_bullet = {
			text = destruction_expert_perk_effect
		}
		if = {
            limit = { culture = { has_cultural_parameter = destruction_xp_rebate } } ###EK EDIT: new parameter
            add_magic_lifestyle_xp = 330
        }
	}
}

destruction_master_perk = {
	lifestyle = magic_lifestyle
	tree = destruction
	position = { 0 4 }
	icon = school_destruction
	
	parent = destruction_expert_perk
	
	effect = {	
		custom_description_no_bullet = {
			text = destruction_master_perk_effect
		}
		if = {
            limit = { culture = { has_cultural_parameter = destruction_xp_rebate } } ###EK EDIT: new parameter
            add_magic_lifestyle_xp = 330
        }
	}
}