version="0.10.3"
tags={
	"Total Conversion"
	"Gameplay"
	"Warfare"
	"Events"
	"Decisions"
	"Culture"
	"Balance"
}
name="Unbound: A Submod for Elder Kings 2"
supported_version="1.8.*"
remote_file_id="2920116721"